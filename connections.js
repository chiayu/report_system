var mysql = require('mysql');
var connectionpool = mysql.createPool({
    host: 'localhost',
    user: 'chiayu',
    password: 'test',
    database: 'report_system',
    acquireTimeout: 1000000,
    debug: ['ComQueryPacket', 'OkPacket']
});

exports.getConnection = function (callback) {
  connectionpool.getConnection(callback);
};
