var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');

var order_forms = require('./routes/order');
var quotation_forms = require('./routes/quotation');
var deal_forms = require('./routes/deal');
var cost_forms = require('./routes/cost');
var client_info = require('./routes/client');
var pay_account = require('./routes/pay_account');
var gen_files = require('./routes/output_files');
var app = express();

// set 

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//set log file
app.use('/', index);
app.use('/users', users);

app.use('/quotation', quotation_forms);
app.use('/order', order_forms);
app.use('/deal', deal_forms);
app.use('/cost', cost_forms);
app.use('/client', client_info);
app.use('/account', pay_account);
app.use('/file', gen_files);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.status(500).send('正在趕工中........!');
  res.render('error');
});


var multer  = require('multer')
var upload = multer({ dest: './uploads/' })


app.post('test/create',upload.single("sampleFile"), function (req, res, next) {
  if (req.file) {
        res.send('文件上传成功')
        console.log(req.file);
        console.log(req.body);
    }
});


module.exports = app;
