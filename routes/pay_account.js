var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var connections = require('../connections');

/* API for account,  PAY_ACCOUNT  */

router.get("/read", function (req, res) {
  res.setHeader('Content-Type', 'application/json');


  //========= CONNECTION START ========
  connections.getConnection(function (err, c) {

    c.query('SELECT * FROM  PAY_ACCOUNT ', function (error, rows) {
      c.release();
      if (error) {
        console.log(error);
        res.status(500).send({
          "msg": "error",
          errorCode: "ERR_PAY_ACC_002"
        });
      } else {
        res.status(200).send(rows);
      }
    });
  });
  //========= CONNECTION END ========

});


module.exports = router;