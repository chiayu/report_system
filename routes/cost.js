var express = require('express');
var auth = require('http-auth')
var router = express.Router();
var mysql = require('mysql');
var connections = require('../connections');

/* Router page. */
router.get('/gen_cost', function (req, res, next) {
  res.render('cost/gen_cost', {
    title: '建立成本檔案'
  });
});
router.get('/gen_cost_type_normal', function (req, res, next) {
  res.render('cost/gen_cost_type_normal', {
    title: '建立成本檔案-一般案件'
  });
});

router.get('/gen_cost_type_mid_process', function (req, res, next) {
  res.render('cost/gen_cost_type_normal', {
    title: '建立成本檔案-中間程序',
    form_type: "mid"
  });
});

router.get('/gen_cost_type_others', function (req, res, next) {
  res.render('cost/gen_cost_type_normal', {
    title: '建立成本檔案-other',
    form_type: "other"
  });
});

router.get('/view_cost', function (req, res, next) {
  res.render('cost/view_cost', {
    title: '選擇成本檔案'
  });
});

var basic = auth.basic({
  realm: "Web."
}, function (username, password, callback) { // Custom authentication method.
  callback(username === "max" && password === "1111");
});
router.get('/revise_cost_detail', auth.connect(basic), function (req, res, next) {
  var pk = "";
  if (req.query != null) {
    pk = req.query.type;
  }
  res.render('cost/revise_cost', {
    title: '修改成本檔案-other',
    id_PK: pk
  });
});


/* API for cost info  */

router.get("/read", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  console.log('SELECT cost_info start.');
  connections.getConnection(function (err, c) {
    // TODO: select only necessary columns
    c.query('SELECT * FROM  QU_COST_INFO ', function (err, rows) {
      c.release();
      if (err) {
        console.log('SELECT cost_info failed');
        console.log(e);
        res.status(500).send({
          "msg": "系統忙碌中"
        });
      } else {
        var json = JSON.stringify(rows);
        rows.forEach(function (element) {
          element.recid = element.ID; //加入recid for前端表格使用
        });

        console.log('SELECT cost_info successfully.');
        return res.status(200).json(rows);
      }
    });
  });
  //get connection end
});


router.get("/read/single/all_values", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  console.log('SELECT SINGLE ALL VALUES in cost_info start.');
  var requestbody = req.body;

  if (req.query.id == undefined) {
    res.status(500).send({
      "msg": "No query parameter: COST_INFO ID."
    });
  }
  connections.getConnection(function (err, c) {

    c.query('SELECT * FROM  QU_COST_INFO WHERE ID = ? ', [req.query.id], function (err, rows) {
      c.release();
      if (err) {
        console.log('SELECT SINGLE cost_info failed(1)');
        console.log(err);
        res.status(500).send({
          "msg": "系統忙碌中"
        });
      } else {
        //var json = JSON.stringify(rows);
        //console.log('JSON-result:', json);
        console.log('SELECT SINGLE cost_info successfully, ID:' + req.query.id);
        res.status(200).send(rows);
      }
    });
  });
  //get connection end
});

router.get("/read/categorys", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  console.log('SELECT CATEGORIES in cost_info start.');

  var query_string = "SELECT ID,TYPE_MAIN,TYPE_PATTERN,TYPE_REGION,TYPE_OTHERS,TYPE_STATUS,MID_STATUS_NAME,MAX_MONTH,MIN_MONTH  FROM  QU_COST_INFO ";

  if (req.query != null) {
    var counter = 0;
    var where_string = " ";
    for (var i in req.query) {
      if (counter > 0) {
        where_string = where_string + " AND " + i + " = '" + req.query[i] + "'";
      } else {
        where_string = " WHERE " + i + " = '" + req.query[i] + "'";
      }
      counter++;
    }
    var order_string = " ORDER BY front_end_order DESC "
    query_string = query_string + " " + where_string + order_string;
    console.log(query_string);
  }

  connections.getConnection(function (err, c) {

    c.query(query_string, function (err, rows) {
      c.release();
      if (err) {
        console.log('SELECT CATEGORIES FROM cost_info failed(1)');
        console.log(err);
        res.status(500).send({
          "msg": "系統忙碌中"
        });
      } else {
        rows.forEach(function (element) {
          element.recid = element.ID;
        });
        console.log('SELECT CATEGORIES FROM cost_info successfully.');
        res.status(200).send(rows);
      }
    });
  });
  // connection string end
});

/**
 * 取得單一成本檔案資料
 */
router.get("/read/single", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  console.log('SELECT SINGLE cost_info start, query id :'+req.query.id);
  
  if (req.query.id == undefined||  ! req.query.id ) {
    console.log("未輸入id");
    res.status(500).send({"msg":"未輸入id"});
  }

  connections.getConnection(function (err, c) {

    c.query(' SELECT ID,TYPE_MAIN,TYPE_PATTERN,TYPE_REGION,TYPE_STATUS,REPORT_PRICE,NOTE,MIN_MONTH,MAX_MONTH,MID_STATUS_NAME,TYPE_OTHERS' +
      ',PLUS_NOTARIZATION,PLUS_INVALIDITY_SEARCH,PLUS_TRANSFER,PLUS_TRANSLATION,PLUS_LARGE_ENTITY,PLUS_SMALL_ENTITY,PLUS_TINY_ENTITY,PLUS_PRIORITY,PLUS_REAL_CHECK,PLUS_SPECIFIC_COUNTRY,GOV_FEE' +
      ' FROM  QU_COST_INFO WHERE ID = ? ', [req.query.id],
      function (err, rows) {
        c.release();
        if (err) {
          console.log('SELECT SINGLE cost_info start fail(1)');
          console.log(err);
          res.status(500).send({ "msg": "系統忙碌中"});
        } else {
          console.log('SELECT SINGLE cost_info end successfully.');
          res.status(200).send(rows);
        }
      });
  });
  // connection string end
});

/**
 * 新增成本檔案
 */
router.post("/create", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  var requestbody = req.body;

  requestbody.type_main = requestbody.type_main == undefined ? "others" : requestbody.type_main;
  requestbody.type_status = requestbody.type_status == undefined ? "others" : requestbody.type_status;
  requestbody.type_pattern = requestbody.type_pattern == undefined ? "others" : requestbody.type_pattern;
  requestbody.type_others = requestbody.type_others == undefined ? "others" : requestbody.type_others;
  requestbody.attorney_fee = requestbody.attorney_fee == "" ? 0 : requestbody.attorney_fee;
  var post = {
    type_main: requestbody.type_main,
    type_pattern: requestbody.type_pattern,
    type_region: requestbody.type_region,
    type_status: requestbody.type_status,
    service_fee: requestbody.service_fee,
    attorney_fee: requestbody.attorney_fee,
    gov_fee: requestbody.gov_fee,
    profit: requestbody.gov_fee,
    tax_fee: 0,
    root_id: requestbody.root_id,
    plus_priority: requestbody.plus_priority,
    plus_real_check: requestbody.plus_real_check,
    plus_specific_country: requestbody.plus_specific_country,
    note: encodeURIComponent(requestbody.note),
    report_price: requestbody.report_price,
    type_others: requestbody.type_others,
    min_month: requestbody.min_month,
    max_month: requestbody.max_month,
    mid_status_name: requestbody.mid_status_name,
    plus_notarization: requestbody.plus_notarization,
    plus_invalidity_search: requestbody.plus_invalidity_search,
    plus_transfer: requestbody.plus_transfer,
    plus_translation: requestbody.plus_translation,
    plus_large_entity: requestbody.plus_large_entity,
    plus_small_entity: requestbody.plus_small_entity,
    plus_tiny_entity: requestbody.plus_tiny_entity,
    front_end_order: get_order_number(requestbody.type_region),


  };
  console.log("新增成本檔案中....");
  connections.getConnection(function (err, c) {
    c.query('INSERT INTO QU_COST_INFO SET ? ,CREATE_DATE=NOW()', post, function (error) {
      c.release();
      if (error) {
        console.log(error.message);
        console.log('失敗:新增成本檔案....');
        res.status(500).send({
          "msg": "請檢查資料庫"
        });
      } else {
        console.log('成功:新增成本檔案....');
        res.status(200).send({
          "msg": "success"
        });
      }
    });
  });
  // connection string end
});

router.post("/updatePartInfo", function (req, res) {
  console.log("UPDATE COST_INFO PARTIAL START.")
  var requestbody = req.body;

  var postArray = [
    encodeURIComponent(requestbody.note),
    requestbody.min_month,
    requestbody.max_month,
    requestbody.plus_priority,
    requestbody.plus_real_check,
    requestbody.plus_specific_country,
    requestbody.id,
  ];
  connections.getConnection(function (err, c) {

    c.query(' UPDATE QU_COST_INFO SET ' +
      ' NOTE = ?, ' +
      ' min_month = ?, ' +
      ' max_month = ?, ' +
      ' plus_priority = ?, ' +
      ' plus_real_check = ?, ' +
      ' plus_specific_country = ? ' +
      ' WHERE id = ?', postArray,
      function (err, results) {
        c.release();
        if (err) {
          console.log("UPDATE COST_INFO failed.")
          console.log(err);
          res.status(500).send({
            "msg": "系統錯誤"
          });
        } else {
          console.log("UPDATE COST_INFO partially successfully.")
          console.log('changed ID=' + requestbody.id + ":" + results.affectedRows + ' rows');
          res.status(200).send({
            "msg": "success",
            "result": results.affectedRows
          });
        }
      });

  });
  // connection string end

});


router.post("/update", function (req, res) {
  console.log("UPDATE COST_INFO  START. [update key] = " + req.body.myPK)
  var requestbody = req.body;

  var hash_update_values = {};
  for (var i in requestbody) {
    var val = requestbody[i];
    if (val != undefined && val != "" && i != "myPK") {
      hash_update_values[i] = val;
    }
  }
  var change_rows = 0;

  connections.getConnection(function (err, c) {

    c.query(' UPDATE QU_COST_INFO SET ? WHERE ID = ? ', [hash_update_values, requestbody.myPK], function (err, results) {
      c.release();
      if (err) {
        console.log("UPDATE COST_INFO ALL failed.");
        console.log("UPDATE valuse:" + hash_update_values);
        console.log(err);
        res.status(500).send({
          "msg": "系統錯誤"
        });
      } else {
        console.log("UPDATE COST_INFO ALL successfully.")
        console.log('changed ID=' + requestbody.myPK + ":" + results.affectedRows + ' rows');
        res.status(200).send({
          "msg": "success",
          "result": results.affectedRows
        });
      }
    });
  });
  // connection string end
});

router.delete("/delete", function (req, res) {
  console.log("DELETE COST_INFO  START. [update key] = " + req.body.myPK)
  var requestbody = req.body;

  connections.getConnection(function (err, c) {
    c.query(' DELETE FROM QU_COST_INFO WHERE ID = ? ', [requestbody.myPK], function (err, results) {
      c.release();
      if (err) {
        console.log("DELETE COST_INFO  failed.");
        console.log(err);
        res.status(500).send({ "msg": "系統錯誤"});
      } else {
        console.log("DELETE COST_INFO ALL successfully.")
        console.log('DELETE ID=' + requestbody.myPK + ":" + results.affectedRows + ' rows');
        res.status(200).send({
          "msg": "success",
          "result": results.affectedRows
        });
      }
    });
  });
  // connection string end
});

function get_order_number(id) {

  if (id == "TWN") {
    return 100;
  } else if (id == "CHN") {
    return 50;
  } else if (id == "JPN") {
    return 20;
  } else if (id == "USA") {
    return 15;
  } else if (id == "ZZZ") {
    return 10;
  } else if (id == "DEU") {
    return 5;
  }

}
module.exports = router;