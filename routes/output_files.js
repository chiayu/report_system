var Docxtemplater = require('docxtemplater');
var JSZip = require('jszip');
var express = require('express');
var path = require('path-resolve');
var router = express.Router();
var fs = require('fs');
var mime = require('mime');

/**
 * 建立報價單
 */
router.post('/quotation', function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    console.log("建立一張報價單......");
    var request_body = req.body;

    //Load the docx file as a binary
    var temp_location = "";
    if (request_body.gen_template_type == "trademark") {
        temp_location = path("./public/files/temp_trademark2.docx", __dirname);
    } else {
        temp_location = path("./public/files/temp2.docx", __dirname);
    }

    console.log("...版型位置:" + temp_location);

    var content = fs.readFileSync(temp_location, "binary");

    console.log("...loading template...");


    // transfer number to comma
    for (var t in request_body.tables) {
        var de_com = decodeURIComponent(request_body.tables[t].form_note).split("\n");
        var tmp_arr = [];
        for(var n in de_com){
            tmp_arr.push({"note_in_line":de_com[n]});
        }
        request_body.tables[t].form_note = tmp_arr;
        request_body.tables[t].form_price = numberWithCommas( request_body.tables[t].form_price);//comma
        if(request_body.tables[t].plus_area){
            request_body.tables[t].plus_area = "(不含有 "+
            numberWithCommas( request_body.tables[t].plus_area) + ")";//comma
        }
    }
    request_body.form_date = request_body.form_date == undefined ? "":request_body.form_date.replace(/-/g,'/');
    
    //comments
    request_body.comment = decodeURIComponent(request_body.comment);
    var comment_note = request_body.comment.split("\n");
    var tmp_com_arr = [];
    for(var n in comment_note){
        tmp_com_arr.push({"note_in_line":comment_note[n]});
    }
    request_body.comments = tmp_com_arr;

    // if tax 
    if (request_body.tax && parseInt(request_body.tax) > 0) {
        request_body.total_price = numberWithCommas(request_body.total_price) + "(含稅NT"+numberWithCommas(request_body.tax)+")";
    }else{
        request_body.total_price = numberWithCommas(request_body.total_price);
    }
    
    var zip = new JSZip(content);
    var doc = new Docxtemplater().loadZip(zip)

    //set the templateVariables
    doc.setData(
        request_body
    );


    //apply them (replace all occurences of {first_name} by Hipp, ...)
    doc.render();
    console.log("render template...");

    var buf = doc.getZip().generate({
        type: "nodebuffer"
    });
    var d = new Date();
    file_name = d.toISOString().substring(0, 10) + d.getUTCMilliseconds() + "_qa.docx";

    var output_file_location = path("./public/output/", __dirname);
    console.log("檔案放置位置:location..." + output_file_location + ":" + file_name);

    fs.writeFileSync(output_file_location + "/" + file_name, buf);
    console.log("write to file...successfully");

    res.status(200).send({ "url": "/output/" + file_name });
});

// 現金收據
router.post('/receipt_cash', function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    var request_body = req.body;
    var temp_location = path("./public/files/receipt_cash.docx", __dirname);
    console.log("tmp location..." + temp_location);
    var content = fs.readFileSync(temp_location, "binary");
    console.log("loading template...");
    var obj = new intToChineseNumberString(false);
    var price_word = obj.getResult(/*number*/request_body.price);
    request_body.price = price_word +"(NT$"+numberWithCommas(request_body.price )+")";
    
    var zip = new JSZip(content);
    var doc = new Docxtemplater().loadZip(zip)

    //set the templateVariables
    doc.setData(
        request_body
    );

    //apply them (replace all occurences of {first_name} by Hipp, ...)
    doc.render();
    console.log("render template...");

    var buf = doc.getZip().generate({
        type: "nodebuffer"
    });
    var d = new Date();
    file_name = d.toISOString().substring(0, 10) + d.getUTCMilliseconds() + "_receipt_cash.docx";
    var output_file_location = path("./public/output/", __dirname);
    console.log("tmp location..." + output_file_location + ":" + file_name);

    fs.writeFileSync(output_file_location + "/" + file_name, buf);
    console.log("write to file...");

    res.send({
        "url": "/output/" + file_name
    });
});


// 報帳收據
router.post('/receipt_for_company', function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    var request_body = req.body;
    var temp_location = path("./public/files/receipt_report.docx", __dirname);
    console.log("tmp location..." + temp_location);
    var content = fs.readFileSync(temp_location, "binary");
    console.log("loading template...");
    var obj = new intToChineseNumberString(false);
    var price_word = obj.getResult(/*number*/request_body.price);
    request_body.price = price_word +"(NT$"+numberWithCommas(request_body.price )+")";
    request_body.pay_status = "全額";
    
    // date  === 
    obj = new intToChineseNumberString(true);
    var d = new Date();
    request_body.year = obj.getResult(d.getUTCFullYear() -1911);
    request_body.month = obj.getResult(d.getMonth()+1);
    request_body.day = obj.getResult(d.getDate());

    var zip = new JSZip(content);
    var doc = new Docxtemplater().loadZip(zip)

    //set the templateVariables
    doc.setData(
        request_body
    );

    //apply them (replace all occurences of {first_name} by Hipp, ...)
    doc.render();
    console.log("render template...");

    var buf = doc.getZip().generate({
        type: "nodebuffer"
    });
    var d = new Date();
    file_name = d.toISOString().substring(0, 10) + d.getUTCMilliseconds() + "_receipt_r.docx";
    var output_file_location = path("./public/output/", __dirname);
    console.log("tmp location..." + output_file_location + ":" + file_name);

    fs.writeFileSync(output_file_location + "/" + file_name, buf);
    console.log("write to file...");

    res.send({
        "url": "/output/" + file_name
    });
});
// 請款單
router.post('/apply_money', function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    var request_body = req.body;
    var temp_location = path("./public/files/apply_money.docx", __dirname);

    var item_tables = req.body.tables;
    var my_item_tables = [];
    //count price
    var discount = 0;
    var tax = 0;

    var total_plus_item = 0;
    for (var i in item_tables) {
        total_plus_item = total_plus_item + parseInt(item_tables[i].a_item_price);
        var i_info = {};    
        i_info['a_item_title'] =     item_tables[i].a_item_title;
        i_info['a_item_price'] =     numberWithCommas(item_tables[i].a_item_price);
        my_item_tables.push(i_info);
    }

    var tax = 0;
    if(req.body.total_tax == "true"){
        var gov_fee = 0;
        if(req.body.total_gov_fee != undefined){
            var gp = parseInt(req.body.total_gov_fee);
            gov_fee = gp;
        }
       var include_tax = (total_plus_item - gov_fee) / 0.9;
       include_tax = Math.ceil(include_tax);
       tax = include_tax -total_plus_item+ gov_fee;
        var i_info = {};    
        i_info['a_item_title'] =     "稅額";
        i_info['a_item_price'] =     numberWithCommas(tax);
        my_item_tables.push(i_info);

    }

    // discount
    var apply_total_money = req.body.item_price;
    discount = apply_total_money - total_plus_item - tax;
    if(discount != 0 ){
         var i_info = {};    
        i_info['a_item_title'] =     "折扣";
        i_info['a_item_price'] =     numberWithCommas(discount);
        my_item_tables.push(i_info);
    }
    request_body.tables = my_item_tables;
    
    console.log("tmp location..." + temp_location);
    var content = fs.readFileSync(temp_location, "binary");
    console.log("loading template...");
    request_body.item_price = numberWithCommas(request_body.item_price );
    var d = new Date();
    request_body.form_date = d.toISOString().substring(0,10).replace(/-/g,'');
    var zip = new JSZip(content);
    var doc = new Docxtemplater().loadZip(zip)

    //set the templateVariables
    doc.setData(
        request_body
    );

    //apply them (replace all occurences of {first_name} by Hipp, ...)
    doc.render();
    console.log("render template...");

    var buf = doc.getZip().generate({
        type: "nodebuffer"
    });
    var d = new Date();
    file_name = d.toISOString().substring(0, 10) + d.getUTCMilliseconds() + "_apply.docx";
    var output_file_location = path("./public/output/", __dirname);
    console.log("tmp location..." + output_file_location + ":" + file_name);

    fs.writeFileSync(output_file_location + "/" + file_name, buf);
    console.log("write to file...");

    res.send({
        "url": "/output/" + file_name
    });
});
function parseValue(value) {

    if (value != "") {
        value = value.replace(",", "");
    } else {
        return 0;
    }

    value = parseInt(value);
    if (!isNaN(value)) {
        return value;
    } else{
        return 0;
    }
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
var intToChineseNumberString =
function (useLowerType) {
    var obj = this;//待回傳的物件
    var numLower = Array('○', '一', '二', '三', '四', '五', '六', '七', '八', '九');//小寫中文
    var numUpper = Array('零', '壹', '貳', '參', '肆', '伍', '陸', '柒', '捌', '玖');//大寫中文
    var numTarget = useLowerType ? numLower : numUpper;//指定要用哪一個中文（大小寫）
    var numLowerUnit1 = Array('','十','百','千');//小寫中文小單位
    var numLowerUnit2 = Array('', '拾', '佰', '仟');//大寫中文小單位
    var numLowerTarget = useLowerType ? numLowerUnit1 : numLowerUnit2;//指定要用哪一個中文（小單位）
    var numLowerUnitLength = 4;//小單位長度
    var numLargeUnit = Array('', '萬', '億', '兆', '京', '垓', '秭', '穰', '溝', '澗', '正', '載', '極');//中文大單位
    var numNegative = "-";//負值
    //判斷是否為數值型態
    obj.checkIsNumber = function (number) {
        return !isNaN(parseInt(number));
    }
    obj.getResult = function (number)
    {
        //如果不是數值，則擲出例外
        if (!this.checkIsNumber(number)) {
            Error("輸入的形態不是Number");
            throw new Error("輸入的形態不是Number");
        }
        //如果是零，則直接輸出
        else if (number == 0) return numTarget[0];
        //轉成字串(使用絕對值避免處理負號)
        var numberString = String(Math.abs(number));
        //準備輸出的字串
        var output = "";
        //小單位(四位數)的傳回值(傳入數字字串)
        var getCurrentPart = function (numString)
        {
            //拆解成陣列
            numString = numString.split("");
            //預設回傳結果
            var result = "";
            //是否輸出過1-9
            var isLastNumberNonZero = false;
            //從後往前掃
            //1011一千○一十一
            for (var index = numString.length - 1; index >= 0 ; index--)
            {
                //目前位數
                var currentDigit = numString.length - index;
                //目前的數字
                var currentNumber = numString[index] - '0';
                //上一個位數為非0
                //剛開始為false(沒有上一個位數)，之後會偵測上一個位數(之前的小位數)是否為非0
                isLastNumberNonZero = ((index + 1) == numString.length) ? false : (numString[index + 1] - '0' > 0);
                //剛開始遇到零不處理，除非有之後遇到1-9
                if (isLastNumberNonZero || currentNumber > 0)
                    result = numTarget[currentNumber]//數字
                        + (currentNumber > 0 ? numLowerTarget[(currentDigit - 1) % numLowerUnitLength] : "")//小單位(個十百千) 大於0才會輸出
                        + result;//上一個位數 
            }
            return result;
        };
        //剛開始小單位長度(前面多出的部份)，Ex 10000，多出的部份為1
        var initialPartLegth = numberString.length % numLowerUnitLength;
        if (initialPartLegth > 0)
            output = getCurrentPart(numberString.substr(0, initialPartLegth)) + numLargeUnit[Math.floor(numberString.length / 4)];
        //之後每次掃四個位數
        for (var i = initialPartLegth; i < numberString.length; i += numLowerUnitLength)
        {
            var partResult = getCurrentPart(numberString.substr(i, numLowerUnitLength));
            output += partResult + (partResult!=""? numLargeUnit[(numberString.length - i) / 4 - 1] : "");
        }
        //回傳時如有負值，則加上-
        return (number < 0 ? numNegative : "") + output;
    }
    //回傳物件
    return  obj;
}
module.exports = router;