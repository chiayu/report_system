var express = require('express');
var router = express.Router();

/* GET quotation page. */
router.get('/view_order', function(req, res, next) {
  res.render('view_order', { title: '報價' });
});
router.get('/gen_order', function(req, res, next) {
  res.render('gen_order', { title: '報價' });
});
module.exports = router;
