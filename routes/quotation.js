var express = require('express');
var router = express.Router();
var connections = require('../connections');

/* GET quotation page. */
router.get('/', function (req, res, next) {
  res.render('gen_quotation', {
    title: '報價'
  });
});

/* GET quotation page. */

router.get('/gen_quotation_pattern', function (req, res, next) {
  res.render('quotation/gen_quotation', {
    title: '報價',
    gen_type: "pattern"
  });
});

router.get('/gen_quotation_trademark', function (req, res, next) {
  res.render('quotation/gen_quotation', {
    title: '報價',
    gen_type: "trademark"
  });
});

router.get('/gen_quotation_others', function (req, res, next) {
  res.render('quotation/gen_quotation', {
    title: '報價',
    gen_type: "others"
  });
});

/* GET quotation page. */
router.get('/view_quotation_unfinished', function (req, res, next) {
  res.render('quotation/view_quotation', {
     load_type: "unfinished"
  });
});

/* GET quotation page. */
router.get('/view_quotation_finished', function (req, res, next) {
  res.render('quotation/view_quotation', {
     load_type: "finished"
  });
});


/* GET quotation page. */
router.get('/edit_quotation', function (req, res, next) {
  // res
  var order_id = req.query['order_id'];

  if(order_id == undefined){
    res.status(500).send({"msg":"error","error_code":"QA_ERR_232", "ui_msg":"沒有輸入訂單號碼"});
  }
  var type_main = req.query['genType'] == undefined ?"pattern": req.query['genType'] ;
  var is_editable = req.query['is_editable'] == undefined ? false: req.query['is_editable'] ;
  res.render('quotation/edit_quotation', {
    gen_type: type_main,
    order_id:order_id,
    is_editable:is_editable,
  });
});


// Router===================================END

router.get("/read", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  console.log('SELECT QU_ORDER start. load_type='+req.query['load_type']);
  var SQL_STRING ="";
  if(req.query['load_type'] == "unfinished"){
     SQL_STRING = "SELECT * FROM  QU_ORDER where order_id not in (select order_id from QU_DEAL)";
  }else if(req.query['load_type'] == "finished"){
    SQL_STRING = "SELECT * FROM  QU_ORDER where order_id  in (select order_id from QU_DEAL)";
  }

  connections.getConnection(function (err, c) {
    c.query(SQL_STRING, function (err, rows) {
      c.release();
      if (err) {
        console.log('SELECT QU_ORDER failed');
        console.log(e);
        res.status(500).send({
          msg: "系統忙碌中",
          error_code:"ERR_QUA_001"
        });
      } else {
        console.log('SELECT QU_ORDER successfully.');
        return res.status(200).json(rows);
      }
    });
  });
  //get connection end
});


// count unfinished qua
router.get("/read/count", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  var SQL_STRING = "SELECT count(*) as count FROM  QU_ORDER where order_id not in (select order_id from QU_DEAL)";
  connections.getConnection(function (err, c) {

    c.query(SQL_STRING, function (err, rows) {
      c.release();
      if (err) {
        res.status(500).send({msg:"error", error_code:"ERR_QUA_009"});
      } else {
        console.log('selecting order.....:');
        return res.status(200).json(rows);
      }
    });
  });
  //get connection end
});


/*  read single qu_order, and how which item in this order */
router.get("/read/single", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  // var requestbody = req.body;

  connections.getConnection(function (err, c) {
    c.query('SELECT * FROM  QU_ORDER WHERE ORDER_ID = ? ', [req.query.order_id], function (err, rows) {
      c.release();
      if (err) {
        res.status(500).send({"msg":"error", "error_code": "QA_ERR_231"})
      } else {
        console.log('selecting QU_ORDER cost info.....ID:' + req.body.order_id);
        res.send(rows);
      }
    });
  });
  //get connection END
});

router.post("/read/order_items/part_cost_info", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  var requestbody = req.body;

  connections.getConnection(function (err, c) {
    c.query('SELECT TYPE_MAIN,TYPE_REGION,TYPE_PATTERN,TYPE_STATUS,MID_STATUS_NAME,TYPE_OTHERS FROM QU_COST_INFO WHERE ID IN (SELECT COST_INFO_ID FROM report_system.QU_ORDER_ITEM WHERE ORDER_ID = ?);', [req.body.order_id], function (err, rows) {
      c.release();
      if (err) {
        res.status(500).send({"msg":"error", "error_code": "QA_ERR_231"})
      } else {
        console.log('selecting QU_ORDER cost info.....ID:' + req.body.order_id);
        res.send(rows);
      }
    });
  });
  //get connection END
});


router.post("/read/order_items/part_cost_info/code", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  var requestbody = req.body;

  connections.getConnection(function (err, c) {
    c.query('SELECT oii.* , ci.GOV_FEE, ci.TYPE_MAIN,ci.TYPE_REGION,ci.TYPE_PATTERN,ci.TYPE_STATUS,ci.MID_STATUS_NAME,ci.TYPE_OTHERS FROM report_system.QU_ORDER_ITEM  oii LEFT JOIN report_system.QU_COST_INFO ci  on oii.COST_INFO_ID = ci.ID WHERE oii.ORDER_ID = ? ;', [req.body.order_id], function (err, rows) {
      c.release();
      if (err) {
        res.status(500).send({"msg":"error", "error_code": "QA_ERR_231"})
      } else {
        console.log('selecting QU_ORDER cost info.....ID:' + req.body.order_id);
        res.send(rows);
      }
    });
  });
  //get connection END
});


router.get("/read/order_items/part_cost_info/code", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  var requestbody = req.body;
  var mysql_string = 'SELECT oii.* , ci.GOV_FEE, ci.PLUS_INVALIDITY_SEARCH as INVALIDITY_SEARCH, ci.PLUS_LARGE_ENTITY as LARGE_ENTITY,'
                    +'ci.PLUS_NOTARIZATION as NOTARIZATION, ci.PLUS_PRIORITY as PRIORITY,'
                    +'ci.PLUS_REAL_CHECK as REAL_CHECK, ci.PLUS_SMALL_ENTITY as SMALL_ENTITY,'
                    +'ci.PLUS_SPECIFIC_COUNTRY as SPECIFIC_COUNTRY, ci.PLUS_TINY_ENTITY as TINY_ENTITY,'
                    +'ci.PLUS_TRANSFER as TRANSFER, ci.PLUS_TRANSLATION as TRANSLATION,'
                    +'ci.TYPE_MAIN,ci.TYPE_REGION,ci.TYPE_PATTERN,ci.TYPE_STATUS,ci.MID_STATUS_NAME,ci.TYPE_OTHERS FROM report_system.QU_ORDER_ITEM  oii LEFT JOIN report_system.QU_COST_INFO ci  on oii.COST_INFO_ID = ci.ID WHERE oii.ORDER_ID = ?;';
  connections.getConnection(function (err, c) {
    c.query(mysql_string, [req.query['id'] ], function (err, rows) {
      c.release();
      if (err) {
        res.status(500).send({"msg":"error", "error_code": "QA_ERR_231"})
      } else {
        console.log('selecting QU_ORDER cost info.....ID:' + req.query['id'] );
        res.send(rows);
      }
    });
  });
  //get connection END
});

router.post("/read/order_items", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  var requestbody = req.body;

  connections.getConnection(function (err, c) {
    c.query('SELECT * FROM report_system.QU_ORDER_ITEM WHERE ORDER_ID = ?', [req.body.order_id], function (err, rows) {
      c.release();
      if (err) {
        res.status(500).send({"msg":"error", "error_code": "QA_ERR_235"})
      } else {
        console.log('selecting QU_ORDER cost info.....ID:' + req.body.order_id);
        res.send(rows);
      }
    });
  });
  //get connection END
});


/* INSERT QU_ORDER. */
router.post('/create/qu_order', function (req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  var requestbody = req.body;
  var post2 = {};
  
  if(requestbody.discount){
    requestbody.discount = requestbody.discount.replace("--","-");
  }

  var client_c = "";
  if(requestbody.company== "" || requestbody.company == undefined){
      client_c = "個人";
  }else{
      client_c = requestbody.company;
  }

  var final_tax = 0;
  if(requestbody.tax != "" && requestbody.tax != undefined){
    final_tax = requestbody.tax;
  }
  var post = {
    order_id: requestbody.time_id ,
    report_price: requestbody.total_price,
    discount: requestbody.discount == undefined ? 0:requestbody.discount,
    tax: final_tax ,
    // account_id: requestbody.pay_account ==undefined? "": requestbody.pay_account ,
    account_text: requestbody.pay_account ==undefined? "": requestbody.pay_account ,
    client_name: requestbody.client ==undefined? "": requestbody.client,
    client_company: client_c ,
    client_phone: requestbody.client_phone ==undefined?"": requestbody.client_phone,
    client_id: requestbody.client_id==undefined ?"": requestbody.client_id ,
    note: requestbody.comment==undefined ?"": requestbody.comment,
    order_type : requestbody.gen_type==undefined ? "others": requestbody.gen_type  

  };

  console.log("inserting.... INTO QU_ORDER 1");
  connections.getConnection(function (err, c) {

    c.query('INSERT INTO QU_ORDER SET ?,CREATE_TIME=NOW()', post, function (error) {
      c.release();
      if (error) {
        console.log(error.message);
        res.status(500).send({msg:"error", error_code:"QUA_0002"});
      } else {
        console.log('success');
        res.status(200).send(JSON.stringify({msg:"success"}));
      }
    });
  });
  // connection string end

})


/* INSERT QU_ORDER. */
router.post('/create/qu_order_items', function (req, res, next) {

  var requestbody = req.body;
  var item_tables = req.body.tables;
  for (var i in item_tables) {
    var my_item = {};
    my_item['order_id'] = requestbody.time_id;
    my_item['cost_info_id'] = item_tables[i].cost_id;
    my_item['item_report_price'] = item_tables[i].form_price;
    var plus_arr = item_tables[i].plus_arr;
    for (var j in plus_arr) {
      var plus_name = plus_arr[j];
      plus_name = plus_name.replace("form_", "");
      my_item[plus_name] = 1;
    }
    insert_one_row_qu_item(my_item, res);
  }

  res.status(200).send({msg:"success"});
})

function insert_one_row_qu_item(item) {
  // FIX ME 一定要存檔成功
  console.log("inserting....INTO QU_ORDER_ITEM");
  connections.getConnection(function (err, c) {

    c.query('INSERT INTO QU_ORDER_ITEM SET ?', item, function (error) {
      c.release();
      if (error) {
        console.log(error.message);
        res.status(500).send({msg:"error", error_code:"QUA_0001"});
      } else {
        console.log('QU_ORDER_ITEM success');
        // res.status(500).send({msg:"success"});
      }
    });
  });
  // connection string end
}



/* UPDATE QU_ORDER. */
router.post('/update/qu_order', function (req, res, next) {

  var requestbody = req.body;
  
  if(requestbody.discount){
    requestbody.discount = requestbody.discount.replace("--","-");
  }
  
  var final_tax = 0;
  if(requestbody.tax != "" && requestbody.tax != undefined){
    final_tax = requestbody.tax;
  }
  var post = {
    // order_id: requestbody.time_id ,
    report_price: requestbody.total_price,
    discount: requestbody.discount == undefined ? 0:requestbody.discount,
    tax: final_tax ,
    // account_id: requestbody.pay_account ==undefined? "": requestbody.pay_account ,
    account_text: requestbody.pay_account ==undefined? "": requestbody.pay_account ,
    client_name: requestbody.client ==undefined? "": requestbody.client,
    client_company: requestbody.company ==undefined? "":requestbody.company ,
    client_phone: requestbody.client_phone ==undefined?"": requestbody.client_phone,
    client_id: requestbody.client_id==undefined ?"": requestbody.client_id ,
    note: requestbody.comment==undefined ?"": requestbody.comment
  };
  var ids = {
    order_id: requestbody.time_id 
  }

  console.log("UPDATE QU_ORDER ....");
  connections.getConnection(function (err, c) {

    c.query('UPDATE QU_ORDER SET ? ,modify_time=NOW() where ?', [post,ids], function (error) {
      c.release();
      if (error) {
        console.log(error.message);
        res.status(500).send({msg:"error", error_code:"QUA_0002"});
      } else {
        console.log('success');
        // res.send({msg:"success"});
      }
    });
  });
  // connection string end

})


/* UPDATE QU_ORDER. STATUS HISTORY */
router.post('/update/qu_order/status', function (req, res, next) {

  var requestbody = req.body; 
  var post = {
    status_history: requestbody.status_history==undefined ?"": requestbody.status_history
  };
  var ids = {
    order_id: requestbody.order_id 
  }

  console.log("UPDATE QU_ORDER SET ....");
  connections.getConnection(function (err, c) {

    c.query('UPDATE QU_ORDER SET ? ,modify_time=NOW() where ?', [post,ids], function (error) {
      c.release();
      if (error) {
        console.log(error.message);
        res.status(500).send({msg:"error", error_code:"QUA_0005"});
      } else {
        console.log('success');
        res.send({msg:"success"});
      }
    });
  });
  // connection string end

})

/* DELETE QU_ORDER_ITEM(for update) */

router.post('/delete/qu_order/items', function (req, res, next) {

  console.log("deleting.....");
  connections.getConnection(function (err, c) {

    c.query('DELETE FROM QU_ORDER_ITEM WHERE ORDER_ID = ?', [req.body.order_id], function (err, rows) {
       c.release();
      if (err) {
        console.log(err.message);
        res.status(500).send({msg:"error", error_code:"QUA_DEL_12"});
      } else {
        console.log('success');
        res.send({msg:"success"});
      }
    });
  });
  // connection string end
})


/**
 * 產收自動產生公司名稱與委託人的下拉選單（來自報價單）
 */
router.get("/read/clients", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  console.log("SELECT clients FROM QU_ORDER START... ");
  var query_string = "SELECT DISTINCT CLIENT_NAME,CLIENT_COMPANY,CLIENT_PHONE,CLIENT_ID FROM QU_ORDER ";

  //  CONNECTION START 
  connections.getConnection(function (err, c) {

    c.query(query_string, function (err, rows) {
      c.release();
      if (err) {
        console.log('SELECT clients failed(1)');
        console.log(err);
        res.status(500).send({
          "msg": "系統忙碌中"
        });
      } else {
        console.log('SELECT clients successfully...size:'+ rows.length);
        res.status(200).send(rows);
      }
    });
  });
  //  CONNECTION END 
});


module.exports = router;