var express = require('express');
var router = express.Router();
var connections = require('../connections');
var path = require('path-resolve');
var multer = require('multer');


var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now())
  }
})

var upload = multer({
  dest: './uploads/',
  storage: storage
});
var fs = require('fs');

/* GET gen deal page. */
router.get('/gen_deal', function (req, res, next) {
  var order_id = req.query['order_id'];
  res.render('deal/gen_deal', {
    order_id: order_id
  });
});

/* GET gen files page. */
router.get('/gen_file', function (req, res, next) {
  var order_id = req.query['order_id'];
  var deal_id = req.query['deal_id'];
  res.render('deal/gen_file', {
    order_id: order_id,
    deal_id: deal_id
  });
});

// =========================================
router.get('/view_deal_all', function (req, res, next) {
  res.render('deal/view_deal', { load_type: 'all'});
});

router.get('/view_deal_not_apply', function (req, res, next) {
  res.render('deal/view_deal', { load_type: 'not_apply'});
});

router.get('/view_deal_not_finished', function (req, res, next) {
  res.render('deal/view_deal', { load_type: 'not_finished'});
});

router.get('/view_deal_not_paid', function (req, res, next) {
  res.render('deal/view_deal', { load_type: 'not_paid'});
});
// =========================================

/* SELECT ALL DEALS */

router.get("/read", function (req, res) {
  res.setHeader('Content-Type', 'application/json');

  var WHERE_SQL = "";

  if(req.query['load_type'] == "not_apply"){
    WHERE_SQL = " WHERE deal.APPLY_STATUS is NULL";
  }else if(req.query['load_type'] == "not_paid"){
    WHERE_SQL = " WHERE deal.APPLY_STATUS is not NULL AND ((deal.PAY_MONEY_FIRST_STATUS is NULL OR deal.PAY_MONEY_SECOND_STATUS is NULL))";
  }else if(req.query['load_type'] == "not_finished"){
    WHERE_SQL = " WHERE deal.DEAL_STATUS is NULL";
  }


  connections.getConnection(function (err, c) {
    // c.query('SELECT PAY_TYPE, PAY_MONEY_SECOND_STATUS,PAY_MONEY_SECOND, PAY_MONEY_FIRST_STATUS, PAY_MONEY_FIRST, ORDER_ID, MODIFY_TIME, ESTIMATE_TIME, DEAL_TITLE, DEAL_STATUS, DEAL_ID, CREATE_TIME, CLIENT_ID, APPLY_STATUS FROM  QU_DEAL ', function (err, rows) {
    c.query('SELECT deal.PAY_TYPE, deal.PAY_MONEY_SECOND_STATUS, deal.PAY_MONEY_SECOND, deal.PAY_MONEY_FIRST_STATUS, deal.PAY_MONEY_FIRST, deal.ORDER_ID, deal.MODIFY_TIME, deal.ESTIMATE_TIME, deal.DEAL_TITLE, deal.DEAL_STATUS, deal.DEAL_ID, deal.CREATE_TIME, deal.CLIENT_ID, deal.APPLY_STATUS, ci.COMPANY_NAME FROM  QU_DEAL deal LEFT JOIN CM_CLIENT_INFO ci on deal.CLIENT_ID = ci.ID  ' + WHERE_SQL, function (err, rows) {
   
      c.release();
      if (err) {
        res.status(500).send({
          msg: "error",
          error_code: "ERR_DEAL_001"
        });
      } else {
        console.log('SELECTING DEAL.....:');
        return res.status(200).json(rows);
      }
    });
  });
  //============ CONNECTION END ==========
});

/*  SELECT SINGLE QU_DEAL */
router.get("/read/single", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  // var requestbody = req.body;

  connections.getConnection(function (err, c) {
    c.query('SELECT * FROM  QU_DEAL WHERE ORDER_ID = ? ', [req.query.order_id], function (err, rows) {
      c.release();
      if (err) {
        res.status(500).send({
          "msg": "error",
          "error_code": "QA_ERR_231"
        })
      } else {
        console.log('SELECTING.. QU_DEAL ID:' + req.body.order_id);
        res.send(rows);
      }
    });
  });
  //============ CONNECTION END ==========
});

/* CREATE A DEAL. */

router.post('/create', upload.single("myfiles"), function (req, res, next) {

  // fs.open(temp_path, 'r', function (status, fd) {
  //   if (status) {
  //       console.log(status.message);
  //       return;
  //   }
  //   var buffer = new Buffer(getFilesizeInBytes(temp_path));
  //   fs.read(fd, buffer, 0, 100, 0, function (err, num) {
  // //   var query ="INSERT INTO `files` (`file_type`, `file_size`, `file`) VALUES ('img', " + getFilesizeInBytes(temp_path) + ",'" + buffer + "' );";
  // //   mySQLconnection.query(query, function (er, da) {
  // //  if (er)throw er;
  // //  });
  //   });
  // });


  if (req.file) {
    // res.status(200).send('文件上傳成功')
    console.log(req.file);
    console.log(req.body);
  }

  // var my_data = req.file.filename;
  var file_location = path("./uploads/" + req.file.filename, __dirname);
  // res.setHeader('Content-Type', 'application/json');
  var requestbody = req.body;
  var buffer = new Buffer(file_location);
  var paytype = requestbody.pay_book_type;
  var post = {
    DEAL_FILE: fs.readFileSync(file_location),
    ORDER_ID: requestbody.order_id,
    DEAL_TITLE: requestbody.deal_title,
    PAY_MONEY_FIRST: requestbody.pay_first,
    PAY_MONEY_SECOND: requestbody.pay_second,
    CLIENT_ID: requestbody.deal_client_id,
    PAY_TYPE : paytype
  };
  console.log("posting....");
  connections.getConnection(function (err, c) {

    c.query('INSERT INTO QU_DEAL SET ?, CREATE_TIME=NOW()', post, function (error) {
      c.release();
      if (error) {
        console.log(error.message);
        res.status(500).send();
      } else {
        console.log('success');
        res.status(200).send({"msg":"succedd"});
      }
    });
  });
  // connection string end

});


/* UPDATE DEAL STATUS */



/* UPDATE QU_ORDER. STATUS HISTORY */
router.post('/update/order_items', function (req, res, next) {

  var requestbody = req.body;
  var post = {
    APPLY_STATUS: true
  };
  var ids = {
    order_id: requestbody.order_id
  }

  console.log("inserting....");
  connections.getConnection(function (err, c) {

    c.query('SELECT *, (SELECT count(*)  FROM QU_ORDER_ITEM  where CODE is not null) AS count FROM QU_ORDER_ITEM qoi, QU_COST_INFO ci WHERE qoi.COST_INFO_ID = ci.ID AND ?', [ids], function (error, rows) {
      // c.release();
      if (error) {
        console.log(error.message);
        res.status(500).send({
          msg: "error",
          error_code: "QU_DEAL_0005"
        });
      }
      console.log(rows);
      var a = new Date();
      var my_date = a.toISOString().substring(2, 10).replace(/-/g, '');
      for (var i in rows) {
        var my_code_seq = rows[i].count;
        my_code_seq = parseInt(my_code_seq) + parseInt(i) + parseInt(1) ;
        rows[i].TYPE_MAIN;
        rows[i].TYPE_REGION;
        var type = map_main[rows[i].TYPE_MAIN];
        var r = map_c_c[rows[i].TYPE_REGION] ? map_c_c[rows[i].TYPE_REGION] : rows[i].TYPE_REGION;
        var my_code = type +  my_date + paddingLeft( my_code_seq ,3) +r;
        
        var posts = {
          code: my_code
        };
        var ids = {
          ITEM_ID: rows[i].ITEM_ID
        }
        c.query('UPDATE QU_ORDER_ITEM SET ? where ?', [posts, ids], function (error, rows) {
          if (error) {
            console.log(error.message);
            res.status(500).send({
              msg: "error",
              error_code: "QU_DEAL_0005"
            });
          }


        });
      }
   
      c.release();
      res.status(200).send({"msg":"success"})
    });
  });
})


/* UPDATE QU_ORDER. DEAL_STATUS  */
router.post('/update/status', function (req, res, next) {

  var requestbody = req.body;
  var post = {
    DEAL_STATUS: true
  };
  var ids = {
    deal_id: requestbody.deal_id
  }

  connections.getConnection(function (err, c) {
    c.query('UPDATE QU_DEAL SET ? ,modify_time=NOW() where ?', [post, ids], function (error) {
      c.release();
      if (error) {
        console.log(error.message);
        res.status(500).send({
          msg: "error",
          error_code: "QU_DEAL_0006"
        });
      } else {
        console.log('success...updating DEAL_STATUS');
        res.send({
          msg: "success"
        });
      }
    });
  });
})


/* UPDATE QU_ORDER. STATUS HISTORY */
router.post('/update/status/apply', function (req, res, next) {

  var requestbody = req.body;
  var post = {
    APPLY_STATUS: true
  };
  var ids = {
    deal_id: requestbody.deal_id
  }

  console.log("inserting....");
  connections.getConnection(function (err, c) {

    c.query('UPDATE QU_DEAL SET ? ,modify_time=NOW() where ?', [post, ids], function (error) {
      c.release();
      if (error) {
        console.log(error.message);
        res.status(500).send({
          msg: "error",
          error_code: "QU_DEAL_0005"
        });
      } else {
        console.log('success...updating');
        res.send({
          msg: "success"
        });
      }
    });
  });
})


/* UPDATE QU_ORDER. PAY FIRST */
router.post('/update/status/pay_first', function (req, res, next) {

  var requestbody = req.body;
  var post = {
    PAY_MONEY_FIRST_STATUS: true
  };
  var ids = {
    deal_id: requestbody.deal_id
  }

  console.log("inserting....");
  connections.getConnection(function (err, c) {

    c.query('UPDATE QU_DEAL SET ? ,modify_time=NOW() where ?', [post, ids], function (error) {
      c.release();
      if (error) {
        console.log(error.message);
        res.status(500).send({
          msg: "error",
          error_code: "QU_DEAL_0006"
        });
      } else {
        console.log('success...updating');
        res.send({
          msg: "success"
        });
      }
    });
  });
})


/* UPDATE QU_ORDER. PAY SECOND */
router.post('/update/status/pay_second', function (req, res, next) {

  var requestbody = req.body;
  var post = {
    PAY_MONEY_SECOND_STATUS: true
  };
  var ids = {
    deal_id: requestbody.deal_id
  }

  console.log("inserting....");
  connections.getConnection(function (err, c) {

    c.query('UPDATE QU_DEAL SET ? ,modify_time=NOW() where ?', [post, ids], function (error) {
      c.release();
      if (error) {
        console.log(error.message);
        res.status(500).send({
          msg: "error",
          error_code: "QU_DEAL_0006"
        });
      } else {
        console.log('success...updating');
        res.send({
          msg: "success"
        });
      }
    });
  });
})

/* API for DEAL  */

// router.get("/read", function (req, res) {
//   res.setHeader('Content-Type', 'application/json');

  
  

//   connections.getConnection(function (err, c) {

//     c.query(SELECT_SQL, function (err, rows) {
//       c.release();
//       if (err) {
//         // callback(err, null);
//         res.send(JSON.stringify({
//           'err': 'err'
//         }));
//       } else {
//         console.log('selecting deal info.....:');
//         return res.json(rows);
//       }
//     });
//   });
//   //get connection end
// });

/* READ COUNT */

router.get("/read/count", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  var SELECT_SQL = "";

  if(req.query['load_type'] == "not_apply"){
    SELECT_SQL = "SELECT count(*) as count FROM  QU_DEAL  WHERE APPLY_STATUS is NULL";
  }else if(req.query['load_type'] == "not_paid"){
    SELECT_SQL = "SELECT count(*) as count FROM  QU_DEAL  WHERE APPLY_STATUS is not NULL AND ((PAY_MONEY_FIRST_STATUS is NULL OR PAY_MONEY_SECOND_STATUS is NULL))";
  }else if(req.query['load_type'] == "not_finished"){
    SELECT_SQL = "SELECT count(*) as count FROM  QU_DEAL  WHERE DEAL_STATUS is NULL";
  }else{
    SELECT_SQL = 'SELECT count(*) as count FROM  QU_DEAL ';
  }
  
  connections.getConnection(function (err, c) {

    c.query(SELECT_SQL, function (err, rows) {
      c.release();
      if (err) {
        // callback(err, null);
        res.send(JSON.stringify({
          'err': 'err'
        }));
      } else {
        console.log('selecting count deal info.....:');
        return res.json(rows);
      }
    });
  });
  //get connection end
});


/*  read single mix qu_order qu_deal by order_id*/
router.get("/read/mix/single", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  var SELECT_SQL = "SELECT * ,deal.CLIENT_ID as CID FROM report_system.QU_DEAL deal ,report_system.QU_ORDER q_order  where deal.order_id = q_order.order_id and  deal.deal_id = ?"
  // var requestbody = req.body;

  connections.getConnection(function (err, c) {
    c.query(SELECT_SQL, [req.query.deal_id], function (err, rows) {
      c.release();
      if (err) {
        res.status(500).send({
          "msg": "error",
          "error_code": "QA_ERR_231"
        })
      } else {
        // fs.writeFile
        var buf = new Buffer(rows[0].DEAL_FILE); // decode
        rows[0].DEAL_FILE = "";
        var file_location = path("./public/temp/" + rows[0].ORDER_ID + ".pdf" , __dirname);
        fs.writeFile(file_location, buf, function(err) {
          if(err) {
            console.log("err", err);
          } else {
            
            // return res.json({'status': 'success'});
          }
        }) 
        
        console.log('selecting QU_ORDER cost info.....ID:' + req.body.order_id);
        res.send(rows);
      }
    });
  });
  //get connection END
});

// Router===================================END



var map_c_c = {
  "TWN": "TW",
  "CHN": "CN",
  "USA": "US",
  "JPN": "JP",
  "KOR": "KR",
  "CAN": "CA",
  "IND": "IN",
  "IOT": "IN",
  "ZZZ": "EP",
  "AUS": "AU",
  "BRA": "BR",
  "EGY": "EG",
  "FRA": "FR",
  "DEU": "DE",
  "GRC": "GR",
  "BEL": "BE",
  "BLR": "RU",
  "RUS": "RU",
  "ITA": "IT",
  "SAU": "SA",
  "MAC": "MO",
  "THA": "TH",
  "HKG": "HK",
  "HUN": "HU",
  "VNM": "VN",
  "SGP": "SG",
  "IDN": "ID",
  "MYS": "MY",
  "GRC": "GR",
  "PRT": "PT",
  "PHL": "PH",
  "VEN": "VE",
  "BRA": "BR",
  "ZAF": "ZA",
  "NLD": "NL",
  "BES": "NL",
  "FIN": "FI",
  "ARG": "AR",
  "ARE": "AE",
  "KWT": "KW",
  "COL": "CO",
  "EGY": "EG",
  "NOR": "NO",
  "URY": "UY",
  "PER": "PE",
  "NZL": "NZ",
  "CZE": "CZ",
  "TUR": "TR",
  "CHL": "CL",
  "AUT": "AT",
  "DNK": "DK",
  "IRL": "IE",
  "CHE": "CH",
  "SWE": "SE",
  "MMR": "MM",
  "MEX": "MX",
  "AUS": "AU",
  "ZZZ": "EPC",
  "CRI": "CR",
  "DOM": "DO",
  "HND": "HN",
  "SAU": "SA"
}

var map_main = {
  'pattern': "P",
  'trademark': 'T',
  'others': 'C'
}

function paddingLeft(str,lenght){
	if(str.length >= lenght)
	return str;
	else
	return paddingLeft("0" +str,lenght);
}
module.exports = router;