 $(function () {

   // 控制表單
   var myPK = $("#cost_info_id").val();
   if (myPK != undefined) {
     $("#id_myPK").val(myPK);
     var apipath = "/cost/read/single/all_values?id=" + myPK;
     var promise = $.getJSON(apipath);

     promise.done(function (result) {
       result = $.grep(result, function (item) {
         // return item.SomeField === filter.SomeField;
         $("#id_type_others").val(result[0].TYPE_OTHERS);

         //main
         var $radios = $("input[name='type_main']");
         $radios.filter('[value=' + result[0].TYPE_MAIN + ']').prop('checked', true);

         //pattern type
         $radios = $("input[name='type_pattern']");
         $radios.filter('[value=' + result[0].TYPE_PATTERN + ']').prop('checked', true);

         // status type
         $radios = $("input[name='type_status']");
         $radios.filter('[value=' + result[0].TYPE_STATUS + ']').prop('checked', true);

         // region
         $('#region option[value="' + result[0].TYPE_REGION + '"]').attr('selected', true);
         $("#id_mid_status_name").val(result[0].MID_STATUS_NAME);
         $("#id_type_others").val(result[0].TYPE_OTHERS);
         $("#id_service_fee").val(result[0].SERVICE_FEE);
         $("#id_gov_fee").val(result[0].GOV_FEE == null ? 0: result[0].GOV_FEE );
         $("#id_attorney_fee").val(result[0].ATTORNEY_FEE == null ? 0 :result[0].ATTORNEY_FEE);
         $("#id_profit").val(result[0].PROFIT  == null ? 0 : result[0].PROFIT );
         // $("#id_tax_fee").val(result[0].TAX_FEE);
         $("#id_report_price").val(result[0].REPORT_PRICE  == null ? 0 :result[0].REPORT_PRICE );
         $("#id_note").val(decodeURIComponent(result[0].NOTE));
         $("#id_min_month").val(result[0].MIN_MONTH);
         $("#id_max_month").val(result[0].MAX_MONTH);
         $("#id_plus_priority").val(result[0].PLUS_PRIORITY);
         $("#id_plus_real_check").val(result[0].PLUS_REAL_CHECK);
         $("#id_plus_specific_country").val(result[0].PLUS_SPECIFIC_COUNTRY);

         $("#id_plus_notarization").val(result[0].PLUS_NOTARIZATION);
         $("#id_plus_invalidity_search").val(result[0].PLUS_INVALIDITY_SEARCH);
         $("#id_plus_transfer").val(result[0].PLUS_TRANSFER);
         $("#id_plus_translation").val(result[0].PLUS_TRANSLATION);
         $("#id_plus_large_entity").val(result[0].PLUS_LARGE_ENTITY);
         $("#id_plus_small_entity").val(result[0].PLUS_SMALL_ENTITY);
         $("#id_plus_tiny_entity").val(result[0].PLUS_TINY_ENTITY);
         console.dir(result[0]);
         
       });

     });

     promise.fail(function () {
       $('body').append('<p>Oh no, something went wrong!</p>');
     });

   }


   //event:顯示專利之次類別
   $("#form_main_type :input").change(function () {
     if ($("#form_pattern_type").parent().length > 0 &&
       ($("#form_main_type input[type='radio']:checked").val() == "pattern")) {
       $("#form_pattern_type").parent().show()
     } else {
       $("#form_pattern_type").parent().hide()
       $('#form_pattern_type').removeAttr('checked');
     }
     console.log(this); // points to the clicked input button
   });


   //event: update成本單據
   $("#update_cost_info").click(function () {

     alert("更新成本檔案中...");
     var promise = $.ajax({
       url: "/cost/update",
       data: $("#cost_info").serialize() + "&myPK=" + $("#cost_info_id").val(),
       type: "POST",
       dataType: 'json'
     });

     promise.done(function (result) {
       alert("成功更新成本檔案");
       location.href = "/cost/view_cost";
     });

     promise.fail(function (result) {
       alert("更新失敗，請洽系統管理員");
     });
   });

   //event: 刪除成本單據
   $("#delete_cost_info").click(function () {
     alert("刪除成本檔案中...");
     var promise = $.ajax({
       url: "/cost/delete",
       data: $("#cost_info").serialize() + "&myPK=" + $("#cost_info_id").val(),
       type: "DELETE",
       dataType: 'json'
     });
     promise.done(function (result) {
       alert("成功 刪除成本檔案");
       location.href = "/cost/view_cost";
     });

     promise.fail(function (result) {
       alert("刪除失敗，請洽系統管理員");
     });
   });

   /*事件:選單開合*/
   $(".sub").click(function () {
     $(this).parent().next().slideToggle(0)
   })

 });


 function refresh_report_price() {
   var result = 0;
   $("#id_service_fee, #id_gov_fee, #id_attorney_fee, #id_profit").each(function (key, value) {
     console.log(this.value);
     var tmp_value = $.isNumeric(this.value) ? this.value : 0;
     result = parseInt(result) + parseInt(tmp_value);
   });
   $("#id_report_price").val(result);
 }