$(function () {

      // 控制表單
      if ($("#form_type").text() == "mid") {
        $("#mid_form_selection").show();
        $(".mid_form").show();
        $(".normal_form").hide();
        $(".other_form").hide();
        $("#cost_info").hide();
        $("#form_title").text("");
        $("#level_3").text("中間程序");
        
      } else if ($("#form_type").text() == "other") {
        $("#form_title").text("產生 其他類別之成本檔案");
        $(".other_form").show();
        $(".normal_form").hide();
        $(".mid_form").hide();
        $("#cost_info").show();
        $("#mid_form_selection").hide();

        $("#level_3").text("其他");
      } else {
        $(".mid_form").hide();
        $(".other_form").hide();
        $(".normal_form").show();
        $("#level_3").text("一般案件");

      }
      //event:顯示專利之次類別
      $("#form_main_type :input").change(function () {
        if ($("#form_pattern_type").parent().length > 0 &&
          ($("#form_main_type input[type='radio']:checked").val() == "pattern")) {
          $("#form_pattern_type").parent().show()
        } else {
          $("#form_pattern_type").parent().hide()
          $('#form_pattern_type').removeAttr('checked');
        }
        console.log(this); // points to the clicked input button
      });


      //event: 加入成本單據
      $("#add_cost_info").click(function () {
        alert("新增中...");

        // $("#cost_info").submit();
        // validateion..........start
        var msg_str = "";
        var is_error = false;
        if($("input[name='type_main']:checked").length  == 0 && $("#form_type").text() =="" ){
          msg_str = msg_str + "請輸入主要類別\n";
          is_error = true;
        }else if ($("input[name='type_main']:checked").length  > 0 && $("input[name='type_main']:checked").val() == "pattern"){
            if($("input[name='type_pattern']:checked").length == 0 ){
                msg_str = msg_str + "請輸入專利類別\n";
                is_error = true;
            }
        }

        if($("#form_type").text() =="mid" && $("#id_mid_status_name").val() ==""){
                msg_str = msg_str + "請輸入中間程序名稱\n";
                is_error = true;
        }

        if($("#form_type").text() =="other" && $("#id_type_others").val() ==""){
                msg_str = msg_str + "請輸入名稱\n";
                is_error = true;
        }

        if($("#region").val() == "XXX" && $("#form_type").text() =="" ){
          msg_str = msg_str + "請輸入國家\n";
            is_error = true;
        }
        
        if($("input[name='type_status']:checked").length == 0 && $("#form_type").text() ==""){
          msg_str = msg_str + "請輸入類別狀態\n";
            is_error = true;
        }
        if($("#id_report_price").val() == "0" || $("#id_report_price").val() ==""){
          msg_str = msg_str + "對外報價不可為0\n";
            is_error = true;
        }

        if(is_error){
          alert(msg_str);
          return ;
        }
        // validateion..........end

        $.ajax({
          url: "/cost/create",
          data: $("#cost_info").serialize(),
          type: "POST",
          dataType: 'json',
          success: function (msg) {
            alert("成功新增");
            location.href = "/cost/view_cost";
          },

          error: function (xhr, ajaxOptions, thrownError) {
            // alert(xhr.status);
            alert("新增失敗");
            alert(thrownError);
          }
        });

      });

      /*事件:選單開合*/
      $(".sub").click(function () {
        $(this).parent().next().slideToggle(0)
      })

    });


    function refresh_report_price() {
      var result = 0;
      $("#id_service_fee, #id_gov_fee, #id_attorney_fee, #id_profit" ).each(function (key, value) {
        console.log(this.value);
        var tmp_value = $.isNumeric(this.value) ? this.value : 0;
        result = parseInt(result) + parseInt(tmp_value);
      });
      $("#id_report_price").val(result);
    }


    function get_country(type) {

      var promise = $.getJSON('/cost/read/categorys');
      var list = [];
      promise.done(function (data) {
        list = data;
        console.log(data);
        if (type != null) {
          var has = {};
          for (var a in list) {
            if (list[a].TYPE_MAIN == type) {
              if (has[list[a].TYPE_REGION] != null) {
                var tmp = has[list[a].TYPE_REGION];
                tmp.push(list[a].TYPE_PATTERN);
                has[list[a].TYPE_REGION] = tmp;
              } else {
                var tmp = [];
                tmp.push(list[a].TYPE_PATTERN);
                has[list[a].TYPE_REGION] = tmp;
              }
            }
          }
          $("#list_country *").remove();
          for (var a in has) {
            var con = country[a];
            var arr_not_uniq = has[a];
            var arr_uniq = {};
            for (var tmp in arr_not_uniq) {
              arr_uniq[arr_not_uniq[tmp]] = 'true';
            }
            console.log(arr_uniq);
            // pattern type
            if (type == "pattern") {
              $("#a_" + type + " #list_country").append("<li><a id='" + a + "'  class='nospace sub' >" + con +
                "</a></li>")
              $("#a_" + type + " #" + a).parent().append("<ul></ul>")
              var options = "";
              for (var ul in arr_uniq) {
                // $("#"+con +" ul").append("<ul>"+type_pattern_map[ul]+"</ul>")
                // $("#"+a).siblings().appendTo("<li><a onClick='alert(aa)' href='#'>"+type_pattern_map[ul]+"</a></li>")
                var tmp = "<ul><a onclick=\"extend_form('" + a + "','" + ul + "')\" >" + type_pattern_map[ul] +
                  "</a></ul>";
                options = options + tmp;
              }
              console.log(options);
              $("#a_" + type + " #" + a).parent().append(options);
            } else {
              $("#a_" + type + " #list_country").append("<li><a id='" + a +
                "'  class='nospace sub' onclick=\"extend_form('" + a + "','" + type + "')\"  >" + con +
                "</a></li>")
              $("#a_" + type + " #" + a).parent().append("<ul></ul>")
            }
          }
        }
      });


    }

    function extend_form(nation, pattern_type) {
      // region type
      var con = country[nation];
      $("#region").val(nation);

      var type = type_pattern_map[pattern_type];
      var main_type = "專利";

      //main type
      var $radios = $("input[name='type_main']");

      if (pattern_type == "trademark") {
        main_type = "商標";
        $radios.filter('[value=trademark]').prop('checked', true);
      } else {
        $radios.filter('[value=pattern]').prop('checked', true);
      }

      //pattern type
      if (type == undefined) {
        type = "";
      } else {
        $radios = $("input[name='type_pattern']");
        $radios.filter('[value=' + pattern_type + ']').prop('checked', true);
      }
      $("#form_title").text("產生中間程序:" + con + "," + type + "," + main_type);
      $("#cost_info").show();
      $("#mid_form_selection").hide();
      $(".other_form").hide();
      // set form submit values
      // main type


      $radios = $("input[name='type_status']");
      $radios.filter('[value=mid_process]').prop('checked', true);

      console.log(nation + ":" + pattern_type);
}