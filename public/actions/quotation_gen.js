 $(function () {
     // document ready ---start
     var today = new Date();
     $("#form_date").val(today.toISOString().substring(0, 10));

     // 產生公司名稱用的auto complete
     var promise = $.getJSON('/quotation/read/clients');
     promise.done(function (data) {
         // comapid_company_name
         // console.log(data);
         var hash_client_name = {};
         var hash_client_company = {};
         var hash_client_phone = {};
         for (var i in data) {
             hash_client_name[data[i].CLIENT_NAME] = true;
             hash_client_company[data[i].CLIENT_COMPANY] = true;
             hash_client_phone[data[i].CLIENT_PHONE] = true;
         }
         $("#id_company_name").autocomplete({
             source: Object.keys(hash_client_company)
         });
         $("#id_client_name").autocomplete({
             source: Object.keys(hash_client_name)
         });
         $("#id_client_phone").autocomplete({
             source: Object.keys(hash_client_phone)
         });
     });

     // 左側選單產生
     var type = "pattern";
     type = $("#gen_type").text();
     if (type == "pattern") {
         $("#gen_word_file_template2").hide();
     }

     var promise = $.getJSON('/cost/read/categorys?type_main=' + type);
     var list = [];
     promise.done(function (data) {
         list = data;
         //console.log(data);
         if (type != null) {
             var has = {};
             for (var a in list) {
                 if (list[a].TYPE_MAIN == type) {
                     if (has[list[a].TYPE_REGION] != null) {
                         var tmp = has[list[a].TYPE_REGION];
                         tmp.push(list[a].TYPE_PATTERN);
                         has[list[a].TYPE_REGION] = tmp;
                     } else {
                         var tmp = [];
                         tmp.push(list[a].TYPE_PATTERN);
                         has[list[a].TYPE_REGION] = tmp;
                     }
                 }

                 if (type == "others") {
                     $("#a_" + type + " #list_others_cost_info").append("<li><a id='" + a +
                         "'  class='nospace sub' onclick=\"select_cost('" + list[a].ID + "')\"  >" + list[a].TYPE_OTHERS +
                         "</a></li>")
                     $("#a_" + type + " #list_others_cost_info").parent().append("<ul></ul>")

                 }
             }
             $("#list_country *").remove();
             for (var a in has) {
                 var con = country[a];
                 var arr_not_uniq = has[a];
                 var arr_uniq = {};
                 for (var tmp in arr_not_uniq) {
                     arr_uniq[arr_not_uniq[tmp]] = 'true';
                 }
                 // console.log(arr_uniq);
                 // pattern type
                 if (type == "pattern") {
                     $("#a_" + type + " #list_country").append("<li><a id='" + a + "'  class='nospace sub' >" + con +
                         "</a></li>")
                     var options = "";
                     for (var ul in arr_uniq) {
                         var tmp = "<li id='" + a + "_" + ul + "'>" +
                             "<a onclick=\"extend_form('" + a + "','" + ul + "')\" >" + type_pattern_map[ul] +
                             "</a></li>";
                         options = options + tmp;
                     }
                     $("#a_" + type + " #" + a).parent().after("<ul></ul>")

                     // console.log(options);
                     $("#a_" + type + " #" + a).parent().next().append(options);
                 } else if (type == "trademark") {
                     $("#a_" + type + " #list_country").append("<li><a id='" + a +
                         "'  class='nospace sub' onclick=\"extend_form_trademark('" + a + "','" + type + "')\"  >" +
                         con +
                         "</a></li>")
                     $("#a_" + type + " #" + a).parent().append("<ul></ul>")
                 }
             }
         }
     });

     /* 取得Event clients 列表 */
     $("#select_client").click(function () {
         console.log("click clients");
         $("#radio_clients label").remove()
         var promise = $.getJSON('/client/read');
         var list = [];
         promise.done(function (data) {
             // console.log(data);
             for (var client in data) {
                 var name = data[client].COMPANY_NAME + ", " + data[client].CLIENT_NAME + ", " + data[client].MPHONE;
                 var value = data[client].ID + "," + name;

                 var radio_html = '<label class="radio">' +
                     '<input type="radio" name="modal_client"' +
                     ' value="' + value + '">' + name + '</label>';
                 $("#radio_clients").append(radio_html);
             }

         });

     });

     /* 取得Event pay_account 列表 */
     $("#select_pay_account").click(function () {
         console.log("click clients");
         $("#radio_accounts label").remove()
         var promise = $.getJSON('/account/read');
         var list = [];
         promise.done(function (data) {
             console.log(data);
             for (var account in data) {
                 var name = data[account].BANK_NAME + ", " + data[account].BANK_ID + ", " +
                     data[account].BANK_BRANCH + ", " + data[account].ACCOUNT_NUM + ", " +
                     data[account].ACCOUNT_NAME;
                 var value = data[account].ID; // + "," + name;

                 var radio_html = '<label class="radio">' +
                     '<input type="radio" name="modal_pay_account"' +
                     ' value="' + value + '">' + name + '</label>';
                 $("#radio_accounts").append(radio_html);
             }

         });

     });



     // event 綁定選取客戶
     $("#btn_select_client").click(function () {
         var $radios = $("input[name='modal_client']");
         if ($radios.filter(":checked").length > 0) {
             var client_value = $radios.filter(":checked").val();
             var infos = client_value.split(",");
             $("#id_company_name").val(infos[1].trim());
             $("#id_client_name").val(infos[2].trim());
             $("#id_client_id").val(infos[0].trim());
             $("#id_client_phone").val(infos[3].trim());
         }
     });

     //event 
     $("#btn_select_pay_account").click(function () {
         var $radios = $("input[name='modal_pay_account']");
         if ($radios.filter(":checked").length > 0) {
             var client_value = $radios.filter(":checked").val();
             var infos = client_value.split(",");
             $("#id_pay_account").val($radios.filter(":checked").parent().text());
             // $("#id_pay_account").val(infos[0].trim());

         }

     });

     // event discount add button
     $("#add_discount").click(function () {
         var num_now = parseInt($("#discount_value").val() == "" ? 0 : $("#discount_value").val())
         $("#discount_value").val(num_now + 1000);
     });
     // event discount minus button
     $("#minus_discount").click(function () {
         var num_now = parseInt($("#discount_value").val() == "" ? 0 : $("#discount_value").val())
         $("#discount_value").val(num_now - 1000);
     });

     //event 
     $("#btn_select_discount").click(function () {
         $(".i_item_discount").remove();
         var html = gen_discount_html("discount", "-" + $("#discount_value").val());
         $("#count_item").append(html);
         sum_up();
     });

     //event 
     $("#btn_cancel_discount").click(function () {
         $(".i_item_discount").remove();
         sum_up();
     });

     function gen_a_row(key, value) {
         return "<div class=\"row\"><div class=\"col-xs-6\">" + key + "</div><div class=\"col-xs-6\">" + value +
             "</div></div>";
     }
     // tax cal
     $("#cal_tax").click(function () {
         if ($("#total_price #sp_total_tax").text() != "") {
             alert("已經加過稅了！！");
             return;
         }
         $("#cal_tax_area .row").remove();
         var total = $("#total_price #sp_total_price").text();
         var totalInt = parseInt(total);
         var total_gov_fee = 0;
         $("#cal_tax_area").append(gen_a_row('目前總金額', total));
         $(".form_gov_fee").each(function (ind, ele) {
             var type = $(ele).parent().find("#form_region").html() + "" + $(ele).parent().find("#form_type").html();
             var fee = $(ele).val();
             total_gov_fee = total_gov_fee + parseInt(fee);
             totalInt = totalInt - parseInt(fee);
             $("#cal_tax_area").append(gen_a_row("扣掉:" + type + "規費:", "-" + fee));

         });

         $("#cal_tax_area").append(gen_a_row("--------", "--------- "));

         if ($(".form_gov_fee").length == 0) {
             $("#cal_tax_area").append(gen_a_row("沒有規費", ""));
         } else {
             $("#cal_tax_area").append(gen_a_row("剩餘", totalInt));
         }
         var no_add_gov_fee = (totalInt / 0.9);
         $("#cal_tax_area").append(gen_a_row("--------", "--------- "));
         $("#cal_tax_area").append(gen_a_row(totalInt + "/ 0.9  = ", no_add_gov_fee));
         var after = (no_add_gov_fee + total_gov_fee);
         var tax = after - parseInt(total);
         $("#cal_tax_area").append(gen_a_row("加回規費" + total_gov_fee + " = ", after));
         $("#cal_tax_area").append(gen_a_row("--------", "--------- "));
         $("#cal_tax_area").append(gen_a_row("本報價稅金(無條件進位)：", "<div id='form_tax'>" + Math.ceil(tax) + "</div>"));
     });

     $("#btn_add_tax").click(function () {
         if ($("#total_price #sp_total_tax").text() != "") {
             alert("已經加過稅了！！");
         } else {
             var tax = parseInt($("#form_tax").html());
             var total = parseInt($("#total_price #sp_total_price").text());
             $("#total_price #sp_total_price").text((tax + total));
             $("#total_price #sp_total_tax").text(" (含稅金NT$" + tax + ")");
         }
     });


     // mminus tax
     $("#btn_minus_tax").click(function () {
         sum_up();
     });
     // gene diff temp
     $("#gen_word_file_template2").click(function () {
         $("#id_gen_template_type").val("trademark");
         $("#gen_word_file").click();
     });

     // gen wordss
     $("#gen_word_file").click(function () {

         // validation start
         var eooro = true;
         var eooro_msg = "";
         if ($("#id_pay_account").val() == "") {
             eooro_msg = "沒有輸入賬戶\n";
             eooro = false;
         }
         if ($("#id_client_name").val() == "") {
             eooro_msg = eooro_msg + "沒有輸入客戶名稱\n";
             eooro = false;
         }
         if ($("#id_company_name").val() == "") {
             // eooro_msg =  eooro_msg + "沒有輸入客戶公司名稱\n";
             // eooro = false;
         }
         if (eooro == false) {
             alert(eooro_msg);
             return;
         }
         // validation end
         var request_info = {};
         var tables = [];
         $(".table").each(function (index, element) {
             var cost_info = {};
             var table_id = element.id;

             cost_info['cost_id'] = table_id.replace("table_", ""); // post
             cost_info['form_type'] = $(element).find("#form_type").text();
             cost_info['form_region'] = $(element).find("#form_region").text();
             cost_info['form_price'] = $(element).find("#form_price input").val(); // post
             cost_info['form_period'] = $(element).find("#form_period").text();
             cost_info['form_note'] = encodeURIComponent($(element).find("#form_note").text());

             var plus_arr = [];
             var plus_area = "";
             if ($(element).find("input[type='checkbox']").length > 0) {
                 var $checkboxes = $(element).find("input[type='checkbox']");
                 $checkboxes.each(function (idx, ele) {
                     if ($(ele).is(':checked')) {
                         plus_arr.push(ele.id);
                         var text = $(ele).parent().next().text();
                         console.log(text);
                         plus_area = plus_area + "\n" + text;
                     }
                 });
             }
             cost_info['plus_arr'] = plus_arr; //post
             cost_info['plus_area'] = plus_area;

             tables.push(cost_info)

         });
         request_info['tables'] = tables;
         //sum up cal 
         var sum_up = [];
         $("#count_item .row").each(function (idx, ele) {
             var item = {};
             var price = $(ele).find(" div .c_price").text();
             if (ele.id == "item_discount") {
                 price = $(".c_discount").text();
                 request_info['discount'] = price; //post
             }
             item = {
                 main_title: $(ele).find(" div:first").text(),
                 main_price: numberWithCommas(price),
                 plus_item: $(ele).find(" ul").text()
             };
             sum_up.push(item);
         });
         request_info['sum_up'] = sum_up;
         request_info['total_price'] = $("#total_price #sp_total_price").text();
         request_info['pay_account'] = $("#id_pay_account").val();
         request_info['comment'] = encodeURIComponent($("#form_comment").val());
         console.dir(request_info);
         request_info['company'] = $("#id_company_name").val();
         request_info['client'] = $("#id_client_name").val();
         request_info['client_id'] = $("#id_client_id").val();
         request_info['client_phone'] = $("#id_client_phone").val();
         request_info['form_date'] = $("#form_date").val();
         request_info['gen_type'] = $("#gen_type").text();
         request_info['tax'] = $("#form_tax").html(); //post
         // time id for order id 
         var d = new Date();
         var time = d.toISOString().substring(0, 10).replace(/-/g, '');
         time = time + d.getHours() + paddingLeft(d.getMinutes(), 3); //+ d.getSeconds();
         request_info['time_id'] = time;
         request_info['gen_template_type'] = $("#id_gen_template_type").val();
         // ajax post
         var promise = $.ajax({

             url: "/file/quotation",
             data: JSON.stringify(request_info),
             type: "POST",
             dataType: 'json',
             contentType: "application/json",
             success: function (msg) {
                 alert("success adding...." + msg.url);
                 location.href = msg.url;
             },

             error: function (xhr, ajaxOptions, thrownError) {
                 // alert(xhr.status);
                 alert(thrownError);
             }
         });

         promise.then(function () {
             console.log("新增訂單 主檔案...");
             post_to_qu(request_info);
         }).then(function () {
             console.log("新增訂單 子檔案...");
             post_to_qu_items(request_info);
         }).then(function () {
             console.log("新增 end...");
         });

     });

     $("#level_3").text(type_main_map[$("#gen_type").text()]);

     $(".btn_select_word").click(function (event, obj) {
         console.log("here" + obj);
         if ($(this).parent() && $(this).parent().prev().text()) {
             var text = $(this).parent().prev().text();
             var old = $("#form_comment").val();
             $("#form_comment").val(old + "\n" + text);
         }
     });

     // document ready ---end


 });

// 新增 訂單主檔
 function post_to_qu(request_info) {

     return $.ajax({
                        url: "/quotation/create/qu_order",
                        data: JSON.stringify(request_info),
                        type: "POST",
                        dataType: 'json',
                        contentType: "application/json",
                        error: function (xhr, ajaxOptions, thrownError) {
                            // alert(xhr.status);
                            alert("系統異常");
                        }
                    });
 }

 // 新增訂單子項目
 function post_to_qu_items(request_info) {

     return $.ajax({
         url: "/quotation/create/qu_order_items",
         data: JSON.stringify(request_info),
         type: "POST",
         dataType: 'json',
         contentType: "application/json",
         success: function (xhr, ajaxOptions, thrownError) {
             alert("你已成功建立一個報價單");
             location.href = "/quotation/view_quotation_unfinished";
         },
         error: function (xhr, ajaxOptions, thrownError) {
             // alert(xhr.status);
             alert("系統異常");
         }

     });
 }

 function get_country(type) {
     if (type != null) {
         var has = {};
         for (var a in list) {
             if (list[a].TYPE_MAIN == type) {
                 if (has[list[a].TYPE_REGION] != null) {
                     var tmp = has[list[a].TYPE_REGION];
                     tmp.push(list[a].TYPE_PATTERN);
                     has[list[a].TYPE_REGION] = tmp;
                 } else {
                     var tmp = [];
                     tmp.push(list[a].TYPE_PATTERN);
                     has[list[a].TYPE_REGION] = tmp;
                 }
             }
         }
         $("#list_country *").remove();
         for (var a in has) {
             var con = country[a];
             var arr_not_uniq = has[a];
             var arr_uniq = {};
             for (var tmp in arr_not_uniq) {
                 arr_uniq[arr_not_uniq[tmp]] = 'true';
             }
             // console.log(arr_uniq);
             // pattern type
             if (type == "pattern") {
                 $("#a_" + type + " #list_country").append("<li><a id='" + a + "'  class='nospace sub' >" + con +
                     "</a></li>")
                 $("#a_" + type + " #" + a).parent().append("<ul></ul>")
                 var options = "";
                 for (var ul in arr_uniq) {
                     // $("#"+con +" ul").append("<ul>"+type_pattern_map[ul]+"</ul>")
                     // $("#"+a).siblings().appendTo("<li><a onClick='alert(aa)' href='#'>"+type_pattern_map[ul]+"</a></li>")
                     var tmp = "<ul><a onclick=\"extend_form('" + a + "','" + ul + "')\" >" + type_pattern_map[ul] +
                         "</a></ul>";
                     options = options + tmp;
                 }
                 // console.log(options);
                 $("#a_" + type + " #" + a).parent().append(options);
             } else {
                 $("#a_" + type + " #list_country").append("<li><a id='" + a +
                     "'  class='nospace sub' onclick=\"extend_form('" + a + "','" + type + "')\"  >" + con + "</a></li>")
                 $("#a_" + type + " #" + a).parent().append("<ul></ul>")
             }
         }
     }

 }

 //select cost
 function select_cost(id) {
     var table_id = "table_" + id;
     if ($("#" + table_id).length > 0) {
         alert("此類型已加入過");
         return;
     }
     $("#table_container").append(gen_table_html(table_id, id));
     var promise = $.getJSON('/cost/read/single?id=' + id);
     var list = [];
     promise.done(function (data) {
         // alert(data);
         var $this_tale = $("#" + table_id + " tbody");
         var name_for_end = "";
         $this_tale.children().remove(); //clear
         // type
         if (data[0].TYPE_MAIN && !data[0].TYPE_OTHERS) {
             var tmp = type_pattern_map[data[0].TYPE_PATTERN] == undefined ? "" : type_pattern_map[data[0].TYPE_PATTERN];
             name_for_end = type_main_map[data[0].TYPE_MAIN] + tmp + type_status_map[data[0].TYPE_STATUS];
             var row_html = gen_row_html("類型", 'form_type', name_for_end, '');
             $this_tale.append(row_html);
         }
         //mid status name
         if (data[0].MID_STATUS_NAME) {
             name_for_end = name_for_end + "," + data[0].MID_STATUS_NAME;
             var row_html = gen_row_html("中間程序名", 'form_mid', data[0].MID_STATUS_NAME, '');
             $this_tale.append(row_html);
         }
         // type others
         if (data[0].TYPE_OTHERS) {
             name_for_end = name_for_end + "," + data[0].TYPE_OTHERS;
             var row_html = gen_row_html("案件名", 'form_type_others', data[0].TYPE_OTHERS, '');
             $this_tale.append(row_html);
         }


         // region
         if (data[0].TYPE_REGION && !data[0].TYPE_OTHERS) {
             var ct = country[data[0].TYPE_REGION];
             name_for_end = ct + " " + name_for_end
             var row_html = gen_row_html("地區", 'form_region', ct, '');
             $this_tale.append(row_html);
         }
         // price
         if (data[0].REPORT_PRICE > 0) {
             var row_html = gen_row_html("申請費", 'form_price', data[0].REPORT_PRICE, 'math', table_id);
             $this_tale.append(row_html);

         }



         // pluses 
         if (data[0].PLUS_NOTARIZATION) {
             var row_html = gen_row_html("公證費", 'form_plus_notarization', data[0].PLUS_NOTARIZATION, 'checkbox',
                 table_id);
             $this_tale.append(row_html);
         }

         if (data[0].PLUS_INVALIDITY_SEARCH) {
             var row_html = gen_row_html("案前檢索費", 'form_plus_invalidity_search', data[0].PLUS_INVALIDITY_SEARCH,
                 'checkbox', table_id);
             $this_tale.append(row_html);
         }
         if (data[0].PLUS_TRANSFER) {
             var row_html = gen_row_html("轉移費", 'form_plus_transfer', data[0].PLUS_TRANSFER, 'checkbox', table_id);
             $this_tale.append(row_html);
         }
         if (data[0].PLUS_TRANSLATION) {
             var row_html = gen_row_html("翻譯費", 'form_plus_translation', data[0].PLUS_TRANSLATION, 'checkbox',
                 table_id);
             $this_tale.append(row_html);
         }
         if (data[0].PLUS_LARGE_ENTITY) {
             var row_html = gen_row_html("大實體", 'form_plus_large_entity', data[0].PLUS_LARGE_ENTITY, 'checkbox',
                 table_id);
             $this_tale.append(row_html);
         }
         if (data[0].PLUS_SMALL_ENTITY) {
             var row_html = gen_row_html("小實體", 'form_plus_small_entity', data[0].PLUS_SMALL_ENTITY, 'checkbox',
                 table_id);
             $this_tale.append(row_html);
         }
         if (data[0].PLUS_TINY_ENTITY) {
             var row_html = gen_row_html("微實體", 'form_plus_tiny_entity', data[0].PLUS_TINY_ENTITY, 'checkbox',
                 table_id);
             $this_tale.append(row_html);
         }
         //
         if (data[0].PLUS_PRIORITY) {
             var row_html = gen_row_html("優先權", 'form_plus_priority', data[0].PLUS_PRIORITY, 'checkbox', table_id);
             $this_tale.append(row_html);


         }
         if (data[0].PLUS_REAL_CHECK) {
             var row_html = gen_row_html("有實審加收", 'form_plus_real_check', data[0].PLUS_REAL_CHECK, 'checkbox',
                 table_id);
             $this_tale.append(row_html);

         }
         if (data[0].PLUS_SPECIFIC_COUNTRY) {
             var row_html = gen_row_html("指定國家", 'form_plus_specific_country', data[0].PLUS_SPECIFIC_COUNTRY,
                 'checkbox', table_id);
             $this_tale.append(row_html);
         }

         // region
         if (data[0].MIN_MONTH) {
             var period = data[0].MIN_MONTH + "~" + data[0].MAX_MONTH + "月份";
             var row_html = gen_row_html("時間", 'form_period', period, '');
             $this_tale.append(row_html);
         }
         // note
         if (data[0].NOTE) {
             var row_html = gen_row_html("說明", 'form_note', decodeURIComponent(data[0].NOTE), 'textarea');
             //data[0].NOTE = data[0].NOTE.replace('%', 'percent');
             $this_tale.append(row_html);
         }


         // gov fee , show for counting tax
         if (data[0].GOV_FEE) {
             var row_html = gen_hidden_row_html('form_gov_fee', data[0].GOV_FEE);
             $this_tale.append(row_html);
         }


         // count_item sum up
         var html = gen_count_item_html(id, name_for_end, '1', data[0].REPORT_PRICE);
         $("#count_item").append(html);

         //re sum_up
         sum_up();

     });


 }


 function gen_discount_html(id, price) {
     var html = "<div class='row i_item_discount' id='item_discount'><div class='col-md-6' >折扣</div>" +
         "<div class='col-md-6'>-<span class='pull-right c_discount'>" + price + "</span></div>" +
         "</div>";
     return html;
 }

 function gen_count_item_html(id, name_for_end, num, price) {
     var html = "<div class='row' id='item_" + id + "'><div class='col-md-6' >" + name_for_end + "<span class='num'>" +
         num + "</span>件</div>" +
         "<div class='col-md-6'><span class='pull-right c_price'>" + price + "</span></div>" +
         "</div>";
     return html;
 }

 function plus_extra(id, c_id, title, money) {
     var html = "<ul class='pull-right " + c_id + "' >" + title + "+<span class='pull-right'' id='plus'>" + money +
         "</ul>"
     $("#count_item #item_" + id).append(html);
     sum_up();

 }

 function sum_up() {
     $("#total_price #sp_total_tax").text("");

     var money = 0;
     $("#form_tax").html("");
     $("#count_item span.pull-right").each(function (index, element) {
         var tmp = $(element).text();
         money = money + parseInt($(element).text());
     });

     $("#total_price #sp_total_price").text(money);
 }

 function extend_form(nation, pattern_type, type) {
     var promise = null;
     if (pattern_type == "trademark") {
         promise = $.getJSON('/cost/read/categorys?type_main=trademark&type_region=' + nation);

     } else {
         promise = $.getJSON('/cost/read/categorys?type_main=pattern&type_region=' + nation + "&type_pattern=" +
             pattern_type);

     }
     var list = [];
     promise.done(function (data) {
         var id = nation + "_" + pattern_type;
         $("#" + id + " ul").remove(); //clear
         $("#" + id).after("<ul></ul>");
         list = data;
         var mid_status = [];

         // 一般狀態
         for (var a in list) {
             var status = list[a].TYPE_STATUS;
             if (status != 'mid_process') {
                 var html = "<ul><a onclick=\"select_cost('" + list[a].ID + "')\" >" + type_status_map[status] +
                     "</a></ul>";
                 $("#" + id).append(html);
             } else {
                 mid_status.push(list[a]);
             }

         }

         //中間程序
         if (mid_status.length > 0) {
             var mid_id = nation + "_" + pattern_type + "_mid_process";
             var html = "<ul id='" + mid_id + "'><a >中間程序</a></ul>";
             $("#" + id).append(html);
             for (var mid in mid_status) {
                 var html = "<ul><a onclick=\"select_cost('" + mid_status[mid].ID + "')\" >" + mid_status[mid].MID_STATUS_NAME +
                     "</a></ul>";
                 $("#" + mid_id).append(html);
             }
         }
     });

 }

 function extend_form_trademark(nation) {
     var promise = $.getJSON('/cost/read/categorys?type_main=trademark&type_region=' + nation);
     var list = [];

     promise.done(function (data) {
         var id = nation;
         $("#" + id + " ul").remove(); //clear
         $("#" + id).after("<ul></ul>");
         list = data;
         var mid_status = [];

         // 一般狀態
         for (var a in list) {
             var status = list[a].TYPE_STATUS;
             if (status != 'mid_process') {
                 var html = "<ul><a onclick=\"select_cost('" + list[a].ID + "')\" >" + type_status_map[status] +
                     "</a></ul>";
                 $("#" + id).append(html);
             } else {
                 mid_status.push(list[a]);
             }

         }

         //中間程序
         if (mid_status.length > 0) {
             var mid_id = nation + "_" + "_mid_process";
             var html = "<ul id='" + mid_id + "'><a >中間程序</a></ul>";
             $("#" + id).append(html);
             for (var mid in mid_status) {
                 var html = "<ul><a onclick=\"select_cost('" + mid_status[mid].ID + "')\" >" + mid_status[mid].MID_STATUS_NAME +
                     "</a></ul>";
                 $("#" + mid_id).append(html);
             }
         }
     });

 }

 function gen_table_html(table_id, index) {

     var html =
         '<div class="col-md-8 column">' +
         '        <table class="table table-bordered table-hover" id="' + table_id + '"> ' +
         "<button type=\"button\" onclick='remove_table(\"" + table_id + "\")' class=\"btn btn-default btn-sm\">" +
         "<span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span>" +
         "</button>" +
         // '               <thead> ' + 
         // '                 <tr> ' + 
         // '                   <th class="text-center col-md-2"> ' + 
         // '                     No. ' + 
         // '                   </th> ' + 
         // '                   <th class="text-center col-md-6"> ' + ($("table").length +1)+
         // '                   </th> ' + 
         // '                 </tr> ' + 
         // '               </thead> ' + 
         '               <tbody> ' +
         '               </tbody> ' +
         '             </table> ' +
         '</div>';
     return html;
 }



 function gen_row_html(name, col_id, value, ctrl_type, table_id) {
     html = "";
     if (ctrl_type == "math") {
         html = '<tr><td>' + name + '</td><td id=' + col_id + '><input type="number" value=' + value + ' disabled>';
         html = html + "<button type=\"button\" onclick=handle_addprice_for_case(\"" + col_id + "\",\"" + table_id +
             "\")" +
             " class=\"btn btn_add btn-default btn-sm\">" +
             "<span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span>" +
             "</button>";
         html = html + "<button type=\"button\" onclick=handle_minusprice_for_case(\"" + col_id + "\",\"" + table_id +
             "\")" +
             " class=\"btn btn_minus btn-default btn-sm\">" +
             "<span class=\"glyphicon glyphicon-minus\" aria-hidden=\"true\"></span>" +
             "</button>";
         html = html + '</td></tr> ';
         return html;
     }

     if (ctrl_type == "textarea") {
         html = '<tr><td>' + name + '</td><td id=' + col_id +
             '> <textarea class="form-control" rows="10" id="comment" disabled>' + value + ' </textarea>';
         html = html + '</td></tr> ';
         return html;
     }

     if (ctrl_type == "checkbox") {
         html = '<tr> <td><input class="form-check-input" onclick=handle_plus("' + col_id + '","' + table_id + '") ' +
             ' type="checkbox" id="' + col_id + '"></td>' +
             '<td><div class=\"form-check form-check-inline\">' +
             '<label class="form-check-label">' +
             '' + name + '<span>' + value + '</span>' +
             '</label></div>'
         html = html + '</td></tr> ';
         return html;
     }

     html = '<tr><td>' + name + '</td><td id=' + col_id + '>' + value + '</td></tr> ';

     return html;
 }

 function gen_hidden_row_html(col_id, value) {
     html = "<input type='hidden' class='" + col_id + "' value=" + value + ">";
     return html;
 }

 function handle_plus(col_id, table_id) {
     var c_name = col_id;
     var id = table_id.replace("table_", "");
     var $this = $("#" + table_id + " #" + col_id);
     var word = $this.parent().next().text().replace(/\d+/, '');
     if ($this.is(':checked')) {
         plus_extra(id, c_name, word, $this.parent().next().find("span").text());
     } else {
         $("." + c_name).remove();
         sum_up();
     }
 }

 function handle_addprice_for_case(col_id, table_id) {
     var c_name = col_id;
     var id = table_id.replace("table_", "");
     var $this = $("#" + table_id + " #" + col_id);
     var price_now = $this.find("input").val()
     num = parseInt(price_now) + 1000;
     $this.find("input").val(num);
     $("#item_" + id + " .c_price").text(num);
     sum_up();
 }

 function handle_minusprice_for_case(col_id, table_id) {
     var c_name = col_id;
     var id = table_id.replace("table_", "");
     var $this = $("#" + table_id + " #" + col_id);
     var price_now = $this.find("input").val()
     num = parseInt(price_now) - 1000;
     $this.find("input").val(num);
     $("#item_" + id + " .c_price").text(num);
     sum_up();
 }

 function remove_table(table_id) {
     $("#" + table_id).parent().remove();
     var tmp_item = "item_" + table_id.replace("table_", "");
     $("#" + tmp_item).remove();
     sum_up();
 }

 function numberWithCommas(x) {
     return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
 }

 function paddingLeft(str, lenght) {
     if (str.length >= lenght)
         return str;
     else
         return paddingLeft("0" + str, lenght);
 }