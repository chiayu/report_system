$(function () {
        // document ready ---start
        // var today = new Date();
        // $("#form_date").val(today.toISOString().substring(0, 10));

        // client info list
        var i = 1;
        $("#add_row").click(function () {
            $('#addr' + i).html("<td>" + (i + 1) + "</td><td><input name='name" + i +
                "' type='text' placeholder='Name' class='form-control input-md'  /> </td><td><input  name='mail" +
                i + "' type='text' placeholder='Mail'  class='form-control input-md'></td><td><input  name='mobile" +
                i + "' type='text' placeholder='Mobile'  class='form-control input-md'></td>");

            $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
            i++;
        });
        $("#delete_row").click(function () {
            if (i > 1) {
                $("#addr" + (i - 1)).html('');
                i--;
            }
        });

        // comapid_company_name
        $("#id_company_name").autocomplete({
            source: ["111", "BBB", "CCC"]
        });

        // 左側選單產生
        var type = "pattern";
        type = $("#gen_type").val();


        var promise = $.getJSON('/cost/read/categorys?type_main=' + type);
        var list = [];
        promise.done(function (data) {
            list = data;
            // console.log(data);
            if (type != null) {
                var has = {};
                for (var a in list) {
                    if (list[a].TYPE_MAIN == type) {
                        if (has[list[a].TYPE_REGION] != null) {
                            var tmp = has[list[a].TYPE_REGION];
                            tmp.push(list[a].TYPE_PATTERN);
                            has[list[a].TYPE_REGION] = tmp;
                        } else {
                            var tmp = [];
                            tmp.push(list[a].TYPE_PATTERN);
                            has[list[a].TYPE_REGION] = tmp;
                        }
                    }

                    if (type == "others") {
                        $("#a_" + type + " #list_others_cost_info").append("<li><a id='" + a +
                            "'  class='nospace sub' onclick=\"select_cost('" + list[a].ID + "')\"  >" + list[a].TYPE_OTHERS +
                            "</a></li>")
                        $("#a_" + type + " #list_others_cost_info").parent().append("<ul></ul>")

                    }
                }
                $("#list_country *").remove();
                for (var a in has) {
                    var con = country[a];
                    var arr_not_uniq = has[a];
                    var arr_uniq = {};
                    for (var tmp in arr_not_uniq) {
                        arr_uniq[arr_not_uniq[tmp]] = 'true';
                    }
                    // console.log(arr_uniq);
                    // pattern type
                    if (type == "pattern") {
                        $("#a_" + type + " #list_country").append("<li><a id='" + a + "'  class='nospace sub' >" + con +
                            "</a></li>")
                        var options = "";
                        for (var ul in arr_uniq) {
                            var tmp = "<li id='" + a + "_" + ul + "'>" +
                                "<a onclick=\"extend_form('" + a + "','" + ul + "')\" >" + type_pattern_map[ul] +
                                "</a></li>";
                            options = options + tmp;
                        }
                        $("#a_" + type + " #" + a).parent().after("<ul></ul>")

                        // console.log(options);
                        $("#a_" + type + " #" + a).parent().next().append(options);
                    } else if (type == "trademark") {
                        $("#a_" + type + " #list_country").append("<li><a id='" + a +
                            "'  class='nospace sub' onclick=\"extend_form_trademark('" + a + "','" + type + "')\"  >" +
                            con +
                            "</a></li>")
                        $("#a_" + type + " #" + a).parent().append("<ul></ul>")
                    }
                }
            }
        });

        promise.then(function () {
            load_order_data($("#order_id").val());
            console.log("1");
        }).then(function () {
            load_item_data($("#order_id").val());
            console.log("2");
        }).then(function () {
            console.log("3");
        });

        /* 取得Event clients 列表 */
        $("#select_client").click(function () {
            console.log("click clients");
            $("#radio_clients label").remove()
            var promise = $.getJSON('/client/read');
            var list = [];
            promise.done(function (data) {
                // console.log(data);
                for (var client in data) {
                    var name = data[client].COMPANY_NAME + ", " + data[client].CLIENT_NAME;
                    var value = data[client].ID + "," + name;

                    var radio_html = '<label class="radio">' +
                        '<input type="radio" name="modal_client"' +
                        ' value="' + value + '">' + name + '</label>';
                    $("#radio_clients").append(radio_html);
                }

            });

        });

        /* 取得Event pay_account 列表 */
        $("#select_pay_account").click(function () {
            console.log("click clients");
            $("#radio_accounts label").remove()
            var promise = $.getJSON('/account/read');
            var list = [];
            promise.done(function (data) {
                console.log(data);
                for (var account in data) {
                    var name = data[account].BANK_NAME + ", " + data[account].BANK_ID + ", " +
                        data[account].BANK_BRANCH + ", " + data[account].ACCOUNT_NUM + ", " +
                        data[account].ACCOUNT_NAME;
                    var value = data[account].ID; // + "," + name;

                    var radio_html = '<label class="radio">' +
                        '<input type="radio" name="modal_pay_account"' +
                        ' value="' + value + '">' + name + '</label>';
                    $("#radio_accounts").append(radio_html);
                }

            });

        });

        // event load 訂單 和訂單資訊

        // event 綁定選取客戶
        $("#btn_select_client").click(function () {
            var $radios = $("input[name='modal_client']");
            if ($radios.filter(":checked").length > 0) {
                var client_value = $radios.filter(":checked").val();
                var infos = client_value.split(",");
                $("#id_company_name").val(infos[1].trim());
                $("#id_client_name").val(infos[2].trim());
                $("#id_client_id").val(infos[0].trim());

            }
        });

        //event
        $("#btn_select_pay_account").click(function () {
            var $radios = $("input[name='modal_pay_account']");
            if ($radios.filter(":checked").length > 0) {
                var client_value = $radios.filter(":checked").val();
                var infos = client_value.split(",");
                $("#id_pay_account").val($radios.filter(":checked").parent().text());
                // $("#id_pay_account").val(infos[0].trim());

            }

        });

        // event discount add button
        $("#add_discount").click(function () {
            var num_now = parseInt($("#discount_value").val() == "" ? 0 : $("#discount_value").val())
            $("#discount_value").val(num_now + 1000);
        });
        // event discount minus button
        $("#minus_discount").click(function () {
            var num_now = parseInt($("#discount_value").val() == "" ? 0 : $("#discount_value").val())
            $("#discount_value").val(num_now - 1000);
        });

        //event
        $("#btn_select_discount").click(function () {
            $(".i_item_discount").remove();
            var html = gen_discount_html("discount", "-" + $("#discount_value").val());
            $("#count_item").append(html);
            sum_up();
        });

        //event
        $("#btn_cancel_discount").click(function () {
            $(".i_item_discount").remove();
            sum_up();
        });

        // tax cal
        $("#cal_tax").click(function () {
            if ($("#total_price #sp_total_tax").text() != "") {
                alert("已經加過稅了！！");
                return;
            }
            $("#cal_tax_area .row").remove();
            var total = $("#total_price #sp_total_price").text();
            var totalInt = parseInt(total);
            var total_gov_fee = 0;
            $("#cal_tax_area").append(gen_a_row('目前總金額', total));
            $(".form_gov_fee").each(function (ind, ele) {
                var type = $(ele).parent().find("#form_region").html() + "" + $(ele).parent().find("#form_type").html();
                var fee = $(ele).val();
                total_gov_fee = total_gov_fee + parseInt(fee);
                totalInt = totalInt - parseInt(fee);
                $("#cal_tax_area").append(gen_a_row("扣掉:" + type + "規費:", "-" + fee));

            });

            $("#cal_tax_area").append(gen_a_row("--------", "--------- "));

            if ($(".form_gov_fee").length == 0) {
                $("#cal_tax_area").append(gen_a_row("沒有規費", ""));
            } else {
                $("#cal_tax_area").append(gen_a_row("剩餘", totalInt));
            }
            var no_add_gov_fee = (totalInt / 0.9);
            $("#cal_tax_area").append(gen_a_row("--------", "--------- "));
            $("#cal_tax_area").append(gen_a_row(totalInt + "/ 0.9  = ", no_add_gov_fee));
            var after = (no_add_gov_fee + total_gov_fee);
            var tax = after - parseInt(total);
            $("#cal_tax_area").append(gen_a_row("加回規費" + total_gov_fee + " = ", after));
            $("#cal_tax_area").append(gen_a_row("--------", "--------- "));
            $("#cal_tax_area").append(gen_a_row("本報價稅金(無條件進位)：", "<div id='form_tax'>" + Math.ceil(tax) + "</div>"));
            $("#id_order_real_tax").val(Math.ceil(tax));
        });

        $("#btn_add_tax").click(function () {
            if ($("#total_price #sp_total_tax").text() != "") {
                alert("已經加過稅了！！");
            } else {
                var tax = 0;
                if ($("#form_tax").html() == undefined || $("#form_tax").html() == "") {
                    tax = parseInt($("#id_order_real_tax").val());
                } else {
                    tax = parseInt($("#form_tax").html());
                }
                var total = parseInt($("#total_price #sp_total_price").text());
                $("#total_price #sp_total_price").text((tax + total));
                $("#total_price #sp_total_tax").text(" (含稅金NT$" + Math.ceil(tax) + ")");
            }
        });
        // mminus tax
        $("#btn_minus_tax").click(function () {
            sum_up();
            $("#id_order_real_tax").val("");
        });

        // gen wordss
        $("#gen_word_file").click(function () {

            var request_info = {};
            var tables = [];
            $(".table").each(function (index, element) {
                var cost_info = {};
                var table_id = element.id;

                cost_info['cost_id'] = table_id.replace("table_", ""); // post
                cost_info['form_type'] = $(element).find("#form_type").text();
                cost_info['form_region'] = $(element).find("#form_region").text();
                cost_info['form_price'] = $(element).find("#form_price input").val(); // post
                cost_info['form_period'] = $(element).find("#form_period").text();
                cost_info['form_note'] = encodeURIComponent($(element).find("#form_note").text());

                var plus_arr = [];
                var plus_area = "";
                if ($(element).find("input[type='checkbox']").length > 0) {
                    var $checkboxes = $(element).find("input[type='checkbox']");
                    $checkboxes.each(function (idx, ele) {
                        if ($(ele).is(':checked')) {
                            plus_arr.push(ele.id);
                            var text = $(ele).parent().next().text();
                            console.log(text);
                            plus_area = plus_area + "\n" + text;
                        }
                    });
                }
                cost_info['plus_arr'] = plus_arr; //post
                cost_info['plus_area'] = plus_area;

                tables.push(cost_info)

            });
            request_info['tables'] = tables;
            //sum up cal
            var sum_up = [];
            $("#count_item .row").each(function (idx, ele) {
                var item = {};
                var price = $(ele).find(" div .c_price").text();
                if (ele.id == "item_discount") {
                    price = $(".c_discount").text();
                    request_info['discount'] = price; //post
                }
                item = {
                    main_title: $(ele).find(" div:first").text(),
                    main_price: price,
                    plus_item: $(ele).find(" ul").text()
                };
                sum_up.push(item);
            });
            request_info['sum_up'] = sum_up;
            request_info['total_price'] = $("#total_price #sp_total_price").text();
            request_info['pay_account'] = $("#id_pay_account").val();
            request_info['comment'] = encodeURIComponent($("#form_comment").val());
            console.dir(request_info);
            request_info['company'] = $("#id_company_name").val();
            request_info['client'] = $("#id_client_name").val();
            request_info['id_client_id'] = $("#id_client_id").val();
            request_info['client_phone'] = $("#id_client_phone").val();
            request_info['form_date'] = $("#form_date").val();
            request_info['gen_type'] = $("#gen_type").val();
            request_info['tax'] = $("#id_order_real_tax").val() ;//$("#form_tax").html(); //post

            // time id for order id
            var d = new Date();
            var time = d.toISOString().substring(0, 10).replace(/-/g, '');
            time = time + d.getHours() + paddingLeft(d.getMinutes(), 3); //+ d.getSeconds();
            request_info['time_id'] = time;

            // ajax post
            var file_url;
            var promise = $.ajax({

                url: "/file/quotation",
                data: JSON.stringify(request_info),
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                success: function (msg) {
                    alert("success adding...." + msg.url);
                    // location.href = msg.url;
                    file_url = msg.url;
                },

                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status);
                    alert(thrownError);
                }
            });

            promise.then(function () {
                console.log("1");
                post_to_qu(request_info);
            }).then(function () {
                console.log("2");
                post_to_qu_items(request_info);
            }).then(function () {
                console.log("3");
                location.href = file_url;
            });

        });

        $("#update_qu_order").click(function () {

            var request_info = {};
            var tables = [];
            $(".table").each(function (index, element) {
                var cost_info = {};
                var table_id = element.id;

                cost_info['cost_id'] = table_id.replace("table_", ""); // post
                cost_info['form_type'] = $(element).find("#form_type").text();
                cost_info['form_region'] = $(element).find("#form_region").text();
                cost_info['form_price'] = $(element).find("#form_price input").val(); // post
                cost_info['form_period'] = $(element).find("#form_period").text();
                cost_info['form_note'] = encodeURIComponent($(element).find("#form_note").text());

                var plus_arr = [];
                var plus_area = "";
                if ($(element).find("input[type='checkbox']").length > 0) {
                    var $checkboxes = $(element).find("input[type='checkbox']");
                    $checkboxes.each(function (idx, ele) {
                        if ($(ele).is(':checked')) {
                            plus_arr.push(ele.id);
                            var text = $(ele).parent().next().text();
                            console.log(text);
                            plus_area = plus_area + "\n" + text;
                        }
                    });
                }
                cost_info['plus_arr'] = plus_arr; //post
                cost_info['plus_area'] = plus_area;

                tables.push(cost_info)

            });
            request_info['tables'] = tables;
            //sum up cal
            var sum_up = [];
            $("#count_item .row").each(function (idx, ele) {
                var item = {};
                var price = $(ele).find(" div .c_price").text();
                if (ele.id == "item_discount") {
                    price = $(".c_discount").text();
                    request_info['discount'] = price; //post
                }
                item = {
                    main_title: $(ele).find(" div:first").text(),
                    main_price: price,
                    plus_item: $(ele).find(" ul").text()
                };
                sum_up.push(item);
            });
            request_info['sum_up'] = sum_up;
            request_info['total_price'] = $("#total_price #sp_total_price").text();
            request_info['pay_account'] = $("#id_pay_account").val();
            request_info['comment'] = encodeURIComponent($("#form_comment").val());
            console.dir(request_info);
            request_info['company'] = $("#id_company_name").val();
            request_info['client'] = $("#id_client_name").val();
            request_info['id_client_id'] = $("#id_client_id").val();
            request_info['client_phone'] = $("#id_client_phone").val();
            request_info['form_date'] = $("#form_date").val();
            request_info['gen_type'] = $("#gen_type").val();
            request_info['tax'] = $("#id_order_real_tax").val();//$("#form_tax").html(); //post

            // time id for order id
            // var d = new Date();
            // var time = d.toISOString().substring(0, 10).replace(/-/g, '');
            // time = time + d.getSeconds();
            // request_info['time_id'] = time;
            request_info['time_id'] = $("#order_id").val();

            // ajax post
            var file_url;
            var promise = $.ajax({

                url: "/file/quotation",
                data: JSON.stringify(request_info),
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                success: function (msg) {
                    alert("success adding...." + msg.url);
                    // location.href = msg.url;
                    file_url = msg.url;
                },

                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status);
                    alert(thrownError);
                }
            });

            promise.then(function () {
                console.log("1.1");
                post_to_update_qu(request_info);
            }).then(function () {
                console.log("2.1");
                delete_qu_items();
            }).then(function () {
                console.log("3.1");
                post_to_qu_items(request_info);
                location.href = file_url;
            });

        });

        var pleaseWait = $('#pleaseWaitDialog');

        showPleaseWait = function () {
            pleaseWait.modal('show');
        };

        hidePleaseWait = function () {
            pleaseWait.modal('hide');
        };

        showPleaseWait();

        $("#level_3").text(type_main_map[$("#gen_type").val()]);
        if ($("#is_editable").val() == "false") {
            $("#update_qu_order").remove();
        }
        // document ready ---end

    });
function gen_a_row(key, value) {
    return "<div class=\"row\"><div class=\"col-xs-6\">" + key + "</div><div class=\"col-xs-6\">" + value +
        "</div></div>";
}

function delete_qu_items() {
    $.ajax({
        url: "/quotation/delete/qu_order/items",
        data: {
            order_id: $("#order_id").val()
        },
        type: "POST",
        dataType: 'json',
        success: function (msg) {
            // alert("success eleting...." + msg);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            // alert(xhr.status);
            if (xhr.responseJSON && xhr.responseJSON.msg != "success") {
                alert(thrownError);
            }
        }
    });
}

function post_to_update_qu(request_info) {

    $.ajax({

        url: "/quotation/update/qu_order",
        data: JSON.stringify(request_info),
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        success: function (msg) {
            alert("success adding...." + msg);
            // location.href = msg.url;
        },

        error: function (xhr, ajaxOptions, thrownError) {
            // alert(xhr.status);
            if (xhr.responseJSON && xhr.responseJSON.msg != "success") {
                alert(thrownError);
            }
        }
    });
}


function post_to_qu(request_info) {

    $.ajax({

        url: "/quotation/create/qu_order",
        data: JSON.stringify(request_info),
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        success: function (msg) {
            alert("success adding...." + msg);
            // location.href = msg.url;
        },

        error: function (xhr, ajaxOptions, thrownError) {
            // alert(xhr.status);
            if (xhr.responseJSON && xhr.responseJSON.msg != "success") {
                alert(thrownError);
            }
        }
    });
}


function post_to_qu_items(request_info) {

    $.ajax({

        url: "/quotation/create/qu_order_items",
        data: JSON.stringify(request_info),
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        success: function (msg) {
            alert("success adding...." + msg);
            // location.href = msg.url;
        },

        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr.responseJSON && xhr.responseJSON.msg != "success") {
                alert(thrownError);
            } else {
                alert("你已成功建立一個報價單");
            }
        }
    });
}

function get_country(type) {
    if (type != null) {
        var has = {};
        for (var a in list) {
            if (list[a].TYPE_MAIN == type) {
                if (has[list[a].TYPE_REGION] != null) {
                    var tmp = has[list[a].TYPE_REGION];
                    tmp.push(list[a].TYPE_PATTERN);
                    has[list[a].TYPE_REGION] = tmp;
                } else {
                    var tmp = [];
                    tmp.push(list[a].TYPE_PATTERN);
                    has[list[a].TYPE_REGION] = tmp;
                }
            }
        }
        $("#list_country *").remove();
        for (var a in has) {
            var con = country[a];
            var arr_not_uniq = has[a];
            var arr_uniq = {};
            for (var tmp in arr_not_uniq) {
                arr_uniq[arr_not_uniq[tmp]] = 'true';
            }
            // console.log(arr_uniq);
            // pattern type
            if (type == "pattern") {
                $("#a_" + type + " #list_country").append("<li><a id='" + a + "'  class='nospace sub' >" + con +
                    "</a></li>")
                $("#a_" + type + " #" + a).parent().append("<ul></ul>")
                var options = "";
                for (var ul in arr_uniq) {
                    // $("#"+con +" ul").append("<ul>"+type_pattern_map[ul]+"</ul>")
                    // $("#"+a).siblings().appendTo("<li><a onClick='alert(aa)' href='#'>"+type_pattern_map[ul]+"</a></li>")
                    var tmp = "<ul><a onclick=\"extend_form('" + a + "','" + ul + "')\" >" + type_pattern_map[ul] +
                        "</a></ul>";
                    options = options + tmp;
                }
                // console.log(options);
                $("#a_" + type + " #" + a).parent().append(options);
            } else {
                $("#a_" + type + " #list_country").append("<li><a id='" + a +
                    "'  class='nospace sub' onclick=\"extend_form('" + a + "','" + type + "')\"  >" + con + "</a></li>")
                $("#a_" + type + " #" + a).parent().append("<ul></ul>")
            }
        }
    }

}

//select cost
function select_cost(id) {
    var table_id = "table_" + id;
    if ($("#" + table_id).length > 0) {
        alert("此類型已加入過");
        return;
    }
    $("#table_container").append(gen_table_html(table_id, id));
    var promise = $.getJSON('/cost/read/single?id=' + id);
    var list = [];
    promise.done(function (data) {
        // alert(data);
        var $this_tale = $("#" + table_id + " tbody");
        var name_for_end = "";
        $this_tale.children().remove(); //clear
        // type
        if (data[0].TYPE_MAIN && !data[0].TYPE_OTHERS) {
            var tmp = type_pattern_map[data[0].TYPE_PATTERN] == undefined ? "" : type_pattern_map[data[0].TYPE_PATTERN];
            name_for_end = type_main_map[data[0].TYPE_MAIN] + tmp + type_status_map[data[0].TYPE_STATUS];
            var row_html = gen_row_html("類型", 'form_type', name_for_end, '');
            $this_tale.append(row_html);
        }
        //mid status name
        if (data[0].MID_STATUS_NAME) {
            name_for_end = name_for_end + "," + data[0].MID_STATUS_NAME;
            var row_html = gen_row_html("中間程序名", 'form_mid', data[0].MID_STATUS_NAME, '');
            $this_tale.append(row_html);
        }
        // type others
        if (data[0].TYPE_OTHERS) {
            name_for_end = name_for_end + "," + data[0].TYPE_OTHERS;
            var row_html = gen_row_html("案件名", 'form_type_others', data[0].TYPE_OTHERS, '');
            $this_tale.append(row_html);
        }


        // region
        if (data[0].TYPE_REGION && !data[0].TYPE_OTHERS) {
            var ct = country[data[0].TYPE_REGION];
            name_for_end = ct + " " + name_for_end
            var row_html = gen_row_html("地區", 'form_region', ct, '');
            $this_tale.append(row_html);
        }
        // price
        if (data[0].REPORT_PRICE > 0) {
            var row_html = gen_row_html("申請費", 'form_price', data[0].REPORT_PRICE, 'math', table_id);
            $this_tale.append(row_html);

        }



        // pluses
        if (data[0].PLUS_NOTARIZATION) {
            var row_html = gen_row_html("公證費", 'form_plus_notarization', data[0].PLUS_NOTARIZATION, 'checkbox',
                table_id);
            $this_tale.append(row_html);
        }

        if (data[0].PLUS_INVALIDITY_SEARCH) {
            var row_html = gen_row_html("案前檢索費", 'form_plus_invalidity_search', data[0].PLUS_INVALIDITY_SEARCH,
                'checkbox', table_id);
            $this_tale.append(row_html);
        }
        if (data[0].PLUS_TRANSFER) {
            var row_html = gen_row_html("轉移費", 'form_plus_transfer', data[0].PLUS_TRANSFER, 'checkbox', table_id);
            $this_tale.append(row_html);
        }
        if (data[0].PLUS_TRANSLATION) {
            var row_html = gen_row_html("翻譯費", 'form_plus_translation', data[0].PLUS_TRANSLATION, 'checkbox',
                table_id);
            $this_tale.append(row_html);
        }
        if (data[0].PLUS_LARGE_ENTITY) {
            var row_html = gen_row_html("大實體", 'form_plus_large_entity', data[0].PLUS_LARGE_ENTITY, 'checkbox',
                table_id);
            $this_tale.append(row_html);
        }
        if (data[0].PLUS_SMALL_ENTITY) {
            var row_html = gen_row_html("小實體", 'form_plus_small_entity', data[0].PLUS_SMALL_ENTITY, 'checkbox',
                table_id);
            $this_tale.append(row_html);
        }
        if (data[0].PLUS_TINY_ENTITY) {
            var row_html = gen_row_html("微實體", 'form_plus_tiny_entity', data[0].PLUS_TINY_ENTITY, 'checkbox',
                table_id);
            $this_tale.append(row_html);
        }
        //
        if (data[0].PLUS_PRIORITY) {
            var row_html = gen_row_html("優先權", 'form_plus_priority', data[0].PLUS_PRIORITY, 'checkbox', table_id);
            $this_tale.append(row_html);


        }
        if (data[0].PLUS_REAL_CHECK) {
            var row_html = gen_row_html("有實審加收", 'form_plus_real_check', data[0].PLUS_REAL_CHECK, 'checkbox',
                table_id);
            $this_tale.append(row_html);

        }
        if (data[0].PLUS_SPECIFIC_COUNTRY) {
            var row_html = gen_row_html("指定國家", 'form_plus_specific_country', data[0].PLUS_SPECIFIC_COUNTRY,
                'checkbox', table_id);
            $this_tale.append(row_html);
        }

        // region
        if (data[0].MIN_MONTH) {
            var period = data[0].MIN_MONTH + "~" + data[0].MAX_MONTH + "月份";
            var row_html = gen_row_html("時間", 'form_period', period, '');
            $this_tale.append(row_html);
        }
        // note
        if (data[0].NOTE) {
            //data[0].NOTE = data[0].NOTE.replace('%','percent');
            var row_html = gen_row_html("說明", 'form_note', decodeURIComponent(data[0].NOTE), 'textarea');
            $this_tale.append(row_html);
        }


        // gov fee , show for counting tax
        if (data[0].GOV_FEE) {
            var row_html = gen_hidden_row_html('form_gov_fee', data[0].GOV_FEE);
            $this_tale.append(row_html);
        }


        // count_item sum up
        var html = gen_count_item_html(id, name_for_end, '1', data[0].REPORT_PRICE);
        $("#count_item").append(html);

        //re sum_up
        sum_up();

    });


}


function gen_discount_html(id, price) {
    var html = "<div class='row i_item_discount' id='item_discount'><div class='col-md-6' >折扣</div>" +
        "<div class='col-md-6'>-<span class='pull-right c_discount'>" + price + "</span></div>" +
        "</div>";
    return html;
}

function gen_count_item_html(id, name_for_end, num, price) {
    var html = "<div class='row' id='item_" + id + "'><div class='col-md-6' >" + name_for_end + "<span class='num'>" +
        num + "</span>件</div>" +
        "<div class='col-md-6'><span class='pull-right c_price'>" + price + "</span></div>" +
        "</div>";
    return html;
}

function plus_extra(id, c_id, title, money) {
    var html = "<ul class='pull-right " + c_id + "' >" + title + "+<span class='pull-right'' id='plus'>" + money +
        "</ul>"
    $("#count_item #item_" + id).append(html);
    sum_up();

}

function sum_up() {
    $("#total_price #sp_total_tax").text("");
    var money = 0;
    $("#form_tax").html("");
    $("#count_item span.pull-right").each(function (index, element) {
        var tmp = $(element).text();
        if (tmp.indexOf('--') != -1) {
            tmp = tmp.replace('--', '-');
        }
        money = money + parseInt(tmp);
    });
    $("#total_price #sp_total_price").text(money);
}

function extend_form(nation, pattern_type, type) {
    var promise = null;
    if (pattern_type == "trademark") {
        promise = $.getJSON('/cost/read/categorys?type_main=trademark&type_region=' + nation);

    } else {
        promise = $.getJSON('/cost/read/categorys?type_main=pattern&type_region=' + nation + "&type_pattern=" +
            pattern_type);

    }
    var list = [];
    promise.done(function (data) {
        var id = nation + "_" + pattern_type;
        $("#" + id + " ul").remove(); //clear
        $("#" + id).after("<ul></ul>");
        list = data;
        var mid_status = [];

        // 一般狀態
        for (var a in list) {
            var status = list[a].TYPE_STATUS;
            if (status != 'mid_process') {
                var html = "<ul><a onclick=\"select_cost('" + list[a].ID + "')\" >" + type_status_map[status] +
                    "</a></ul>";
                $("#" + id).append(html);
            } else {
                mid_status.push(list[a]);
            }

        }

        //中間程序
        if (mid_status.length > 0) {
            var mid_id = nation + "_" + pattern_type + "_mid_process";
            var html = "<ul id='" + mid_id + "'><a >中間程序</a></ul>";
            $("#" + id).append(html);
            for (var mid in mid_status) {
                var html = "<ul><a onclick=\"select_cost('" + mid_status[mid].ID + "')\" >" + mid_status[mid].MID_STATUS_NAME +
                    "</a></ul>";
                $("#" + mid_id).append(html);
            }
        }
    });

}

function extend_form_trademark(nation) {
    var promise = $.getJSON('/cost/read/categorys?type_main=trademark&type_region=' + nation);
    var list = [];

    promise.done(function (data) {
        var id = nation;
        $("#" + id + " ul").remove(); //clear
        $("#" + id).after("<ul></ul>");
        list = data;
        var mid_status = [];

        // 一般狀態
        for (var a in list) {
            var status = list[a].TYPE_STATUS;
            if (status != 'mid_process') {
                var html = "<ul><a onclick=\"select_cost('" + list[a].ID + "')\" >" + type_status_map[status] +
                    "</a></ul>";
                $("#" + id).append(html);
            } else {
                mid_status.push(list[a]);
            }

        }

        //中間程序
        if (mid_status.length > 0) {
            var mid_id = nation + "_" + pattern_type + "_mid_process";
            var html = "<ul id='" + mid_id + "'><a >中間程序</a></ul>";
            $("#" + id).append(html);
            for (var mid in mid_status) {
                var html = "<ul><a onclick=\"select_cost('" + mid_status[mid].ID + "')\" >" + mid_status[mid].MID_STATUS_NAME +
                    "</a></ul>";
                $("#" + mid_id).append(html);
            }
        }
    });

}

function gen_table_html(table_id, index) {

    var html =
        '<div class="col-md-8 column">' +
        '        <table class="table table-bordered table-hover" id="' + table_id + '"> ' +
        "<button type=\"button\" onclick='remove_table(\"" + table_id + "\")' class=\"btn btn-default btn-sm\">" +
        "<span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span>" +
        "</button>" +
        // '               <thead> ' +
        // '                 <tr> ' +
        // '                   <th class="text-center col-md-2"> ' +
        // '                     No. ' +
        // '                   </th> ' +
        // '                   <th class="text-center col-md-6"> ' + ($("table").length +1)+
        // '                   </th> ' +
        // '                 </tr> ' +
        // '               </thead> ' +
        '               <tbody> ' +
        '               </tbody> ' +
        '             </table> ' +
        '</div>';
    return html;
}



function gen_row_html(name, col_id, value, ctrl_type, table_id) {
    html = "";
    if (ctrl_type == "math") {
        html = '<tr><td>' + name + '</td><td id=' + col_id + '><input type="number" value=' + value + ' disabled>';
        html = html + "<button type=\"button\" onclick=handle_addprice_for_case(\"" + col_id + "\",\"" + table_id +
            "\")" +
            " class=\"btn btn_add btn-default btn-sm\">" +
            "<span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span>" +
            "</button>";
        html = html + "<button type=\"button\" onclick=handle_minusprice_for_case(\"" + col_id + "\",\"" + table_id +
            "\")" +
            " class=\"btn btn_minus btn-default btn-sm\">" +
            "<span class=\"glyphicon glyphicon-minus\" aria-hidden=\"true\"></span>" +
            "</button>";
        html = html + '</td></tr> ';
        return html;
    }

    if (ctrl_type == "textarea") {
        html = '<tr><td>' + name + '</td><td id=' + col_id +
            '> <textarea class="form-control" rows="10" id="comment" disabled>' + value + ' </textarea>';
        html = html + '</td></tr> ';
        return html;
    }

    if (ctrl_type == "checkbox") {
        html = '<tr> <td><input class="form-check-input" onclick=handle_plus("' + col_id + '","' + table_id + '") ' +
            ' type="checkbox" id="' + col_id + '"></td>' +
            '<td><div class=\"form-check form-check-inline\">' +
            '<label class="form-check-label">' +
            '' + name + '<span>' + value + '</span>' +
            '</label></div>'
        html = html + '</td></tr> ';
        return html;
    }

    html = '<tr><td>' + name + '</td><td id=' + col_id + '>' + value + '</td></tr> ';

    return html;
}

function gen_hidden_row_html(col_id, value) {
    html = "<input type='hidden' class='" + col_id + "' value=" + value + ">";
    return html;
}

function handle_plus(col_id, table_id) {
    var c_name = col_id;
    var id = table_id.replace("table_", "");
    var $this = $("#" + table_id + " #" + col_id);
    var word = $this.parent().next().text().replace(/\d+/, '');
    if ($this.is(':checked')) {
        plus_extra(id, c_name, word, $this.parent().next().find("span").text());
    } else {
        $("." + c_name).remove();
        sum_up();
    }
}

function handle_addprice_for_case(col_id, table_id, after_change) {
    var c_name = col_id;
    var id = table_id.replace("table_", "");
    var $this = $("#" + table_id + " #" + col_id);
    var price_now = "0";
    var num = 0;
    if (after_change) {
        num = parseInt(after_change);
    } else {
        var price_now = $this.find("input").val()
        num = parseInt(price_now) + 1000;
    }

    $this.find("input").val(num);
    $("#item_" + id + " .c_price").text(num);
    sum_up();
}

function handle_minusprice_for_case(col_id, table_id) {
    var c_name = col_id;
    var id = table_id.replace("table_", "");
    var $this = $("#" + table_id + " #" + col_id);
    var price_now = $this.find("input").val()
    num = parseInt(price_now) - 1000;
    $this.find("input").val(num);
    $("#item_" + id + " .c_price").text(num);
    sum_up();
}

function remove_table(table_id) {
    $("#" + table_id).parent().remove();
    var tmp_item = "item_" + table_id.replace("table_", "");
    $("#" + tmp_item).remove();
    sum_up();
}

function load_item_data(order_id) {
    var d = $.Deferred();
    $.ajax({
        type: "POST",
        url: "/quotation/read/order_items",
        data: {
            order_id: order_id
        },
        dataType: "json"
    }).then(function (result) {
        // clean
        // $("#order_item_list").children().remove();
        for (var i in result) {
            var item_name = "";
            select_cost(result[i].COST_INFO_ID);
        }
        console.log("here is itmes in  order..." + result);
        d.resolve(result);
        return result;
    }).then(function (result) {

        //  alert("second");
        setTimeout(function () {
            for (var i in result) {
                var table_id = "table_" + result[i].COST_INFO_ID;
                if (result[i].ITEM_REPORT_PRICE != $("#" + table_id + " #form_price input").val()) {
                    handle_addprice_for_case('form_price', table_id, result[i].ITEM_REPORT_PRICE);
                    $("#" + table_id + " #form_price input").val(result[i].ITEM_REPORT_PRICE);
                }

                if (result[i].PLUS_NOTARIZATION) {
                    $("#" + table_id + " #form_plus_notarization").prop("checked", true);
                    handle_plus("form_plus_notarization", table_id);
                }
                if (result[i].PLUS_INVALIDITY_SEARCH) {
                    $("#" + table_id + " #form_plus_invalidity_search").prop("checked", true);
                    handle_plus("form_plus_invalidity_search", table_id);
                }
                if (result[i].PLUS_TRANSFER) {
                    $("#" + table_id + " #form_plus_transfer").prop("checked", true);
                    handle_plus("form_plus_transfer", table_id);
                }
                if (result[i].PLUS_TRANSLATION) {
                    $("#" + table_id + " #form_plus_translation").prop("checked", true);
                    handle_plus("form_plus_translation", table_id);
                }
                if (result[i].PLUS_LARGE_ENTITY) {
                    $("#" + table_id + " #form_plus_large_entity").prop("checked", true);
                    handle_plus("form_plus_large_entity", table_id);
                }
                if (result[i].PLUS_SMALL_ENTITY) {
                    $("#" + table_id + " #form_plus_small_entity").prop("checked", true);
                    handle_plus("form_plus_small_entity", table_id);
                }
                if (result[i].PLUS_TINY_ENTITY) {
                    $("#" + table_id + " #form_plus_tiny_entity").prop("checked", true);
                    handle_plus("form_plus_tiny_entity", table_id);
                }
                if (result[i].PLUS_PRIORITY) {
                    $("#" + table_id + " #form_plus_priority").prop("checked", true);
                    handle_plus("form_plus_priority", table_id);
                }
                if (result[i].PLUS_REAL_CHECK) {
                    $("#" + table_id + " #form_plus_real_check").prop("checked", true);
                    handle_plus("form_plus_real_check", table_id);
                }
                if (result[i].PLUS_SPECIFIC_COUNTRY) {
                    $("#" + table_id + " #form_plus_specific_country").prop("checked", true);
                    handle_plus("form_plus_specific_country", table_id);
                }

            }

            load_discount_and_tax();
        }, 2500);




    });

}

function load_discount_and_tax() {
    if ($("#discount_value").val() != "" && $("#discount_value").val() != "0") {
        $("#btn_select_discount").click();
    }
    if ($("#id_order_real_tax").val() != "") {
        $("#btn_add_tax").click();
    }

    // init cal_tax when edit
    $("#cal_tax_area .row").remove();
    var total = $("#total_price #sp_total_price").text();
    var totalInt = parseInt(total);
    var tax = 0;
    if ($("#id_order_real_tax").val() != ""){
        tax = parseInt($("#id_order_real_tax").val());
        totalInt = totalInt - tax;
        // totalInt = totalInt - $("#id_order_real_tax").val();
    }
    var total_gov_fee = 0;
    $("#cal_tax_area").append(gen_a_row('目前總金額', totalInt));
    $(".form_gov_fee").each(function (ind, ele) {
        var type = $(ele).parent().find("#form_region").html() + "" + $(ele).parent().find("#form_type").html();
        var fee = $(ele).val();
        total_gov_fee = total_gov_fee + parseInt(fee);
        totalInt = totalInt - parseInt(fee);
        $("#cal_tax_area").append(gen_a_row("扣掉:" + type + "規費:", "-" + fee));

    });

    $("#cal_tax_area").append(gen_a_row("--------", "--------- "));

    if ($(".form_gov_fee").length == 0) {
        $("#cal_tax_area").append(gen_a_row("沒有規費", ""));
    } else {
        $("#cal_tax_area").append(gen_a_row("剩餘", totalInt));
    }
    var no_add_gov_fee = (totalInt / 0.9);
    $("#cal_tax_area").append(gen_a_row("--------", "--------- "));
    $("#cal_tax_area").append(gen_a_row(totalInt + "/ 0.9  = ", no_add_gov_fee));
    var after = (no_add_gov_fee + total_gov_fee);
    // var tax = after - parseInt(total);
    $("#cal_tax_area").append(gen_a_row("加回規費" + total_gov_fee + " = ", after));
    $("#cal_tax_area").append(gen_a_row("--------", "--------- "));
    $("#cal_tax_area").append(gen_a_row("本報價稅金(無條件進位)：", "<div id='form_tax'>" + Math.ceil(tax) + "</div>"));
    $("#id_order_real_tax").val(Math.ceil(tax));

    // $(".progress").remove();
    $('#pleaseWaitDialog').modal('hide');
}

function load_order_data(order_id) {
    var d = $.Deferred();
    var promise = $.getJSON("/quotation/read/single?order_id=" + order_id);
    promise.then(function (result) {
        // clean
        // $("#order_item_list").children().remove();
        for (var i in result) {
            console.dir(result[i]);
            $("#id_company_name").val(result[i].CLIENT_COMPANY);
            $("#id_client_name").val(result[i].CLIENT_NAME);
            $("#id_client_phone").val(result[i].CLIENT_PHONE);
            if (result[i].CREATE_TIME != undefined && result[i].CREATE_TIME.length > 10) {
                $("#form_date").val(result[i].CREATE_TIME.substring(0, 10));
            }
            // load discount;
            if (result[i].TAX || parseInt(result[i].TAX) > 0) {
                $("#form_tax").html(result[i].TAX);
                $("#id_order_real_tax").val(result[i].TAX);
            }
            $("#form_comment").text(decodeURIComponent(result[i].NOTE) == undefined ? "" : decodeURIComponent(result[
                i].NOTE));
            $("#discount_value").val(result[i].DISCOUNT);
            $("#id_pay_account").val(result[i].ACCOUNT_TEXT);

        }
        console.log("here is itmes in  order..." + result);
        d.resolve(result);
        return result;
    });
}

function paddingLeft(str, lenght) {
    if (str.length >= lenght)
        return str;
    else
        return paddingLeft("0" + str, lenght);
}