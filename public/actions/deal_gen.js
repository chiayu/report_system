$(function () {


    // =============DOCUMENT READY START
    // date
    var today = new Date();
    $("#form_date").val(today.toISOString().substring(0, 10));

    // load data......
    var order_id = $("#order_id").val();
    $("#id_order_id").val(order_id);
    var d = $.Deferred();
    var promise = $.getJSON("/quotation/read/single?order_id=" + order_id);
    promise.then(function (result) {
        // clean
        // $("#order_item_list").children().remove();
        var result_client_info_id;
        for (var i in result) {
            $("#id_order_report_price").val(result[i].REPORT_PRICE);
            result_client_info_id = result[i].CLIENT_ID;
            $("#id_deal_client_id").val(result_client_info_id);
            $("#id_deal_client_company").val(result[i].CLIENT_COMPANY);
            $("#id_deal_client_name").val(result[i].CLIENT_NAME);
            $("#id_deal_client_phone").val(result[i].CLIENT_PHONE);
            $("#id_pay_first").val(result[i].REPORT_PRICE);
            // $("#id_deal_client_company_code").val(result[i].CLIENT_COMPANY);

        }
        console.log("here is itmes in  order..." + result);
        d.resolve(result_client_info_id);
        return d;

    }).then(function (result) {

        // if (result[0].CLIENT_ID) {
        if (result) {
            console.log("load client info.....");
            load_client_info(result[0].CLIENT_ID);
            // }
        }
    });

    $("#confirm_deal").click(function () {
        $.blockUI({ message: '<h2> 進入委任中，請稍候...</h2>' });
        console.log("uploading....");
        // validation....!
        var err_msg = "";
        var errore = false;

        if ($("#id_deal_title").val() == "") {
            errore = true;
            err_msg = err_msg + "請輸入案件名稱\n";
        }

        var pay_total = parseFloat($("#id_pay_first").val()) + parseFloat($("#id_pay_second").val());
        var totoal_price = parseFloat($("#id_order_report_price").val());

        if (pay_total != totoal_price) {
            errore = true;
            err_msg = err_msg + "金額有誤\n";
        }

        if ($("#files")[0].files.length < 1) {
            errore = true;
            err_msg = err_msg + "檔案尚未上傳\n";
        }

        if (errore) {
            alert(err_msg);
            $.unblockUI();
            return;
        }

        // validation end

        var formData = new FormData($("#deal_info")[0]);

        var promise = $.ajax({
            url: '/deal/create', //Server script to process data
            type: 'POST',
            data: formData,
            cache: false,
            // contentType: 'multipart/form-data',
            contentType: false,
            processData: false,
            success: function (msg) {
                $.unblockUI();
                alert("成功轉成委任檔.");
            },

            error: function (xhr, ajaxOptions, thrownError) {
                $.unblockUI();
                alert("未知的錯誤:" + thrownError);
            }
        });
        promise.then(function () {
            console.log("1");
            update_qu_item($("#id_order_id").val());
        }).then(function () {
            save_client_info();

        })
        // $("#deal_info").submit();
    });


    // radio ===
    //event:
    $("#div_pay_book_type :input").change(function () {
        var price_pay_first = parseInt($("#id_order_report_price").val());
        if ($("#div_pay_book_type").parent().length > 0 &&
            ($("#div_pay_book_type input[type='radio']:checked").val() == "all")) {
            $("#id_pay_first").val(price_pay_first);
            count_pay_second();
        } else if ($("#div_pay_book_type input[type='radio']:checked").val() == "half") {
            // $("#form_pattern_type").parent().hide()
            $("#id_pay_first").val((price_pay_first / 2));
            count_pay_second();
        } else if ($("#div_pay_book_type input[type='radio']:checked").val() == "other") {

        }
        console.log(this); // points to the clicked input button
    });

    var $radios = $('input:radio[name=pay_book_type]');
    if ($radios.is(':checked') === false) {
        $radios.filter('[value=all]').prop('checked', true);

    }
    // document ready ---end

});

function count_pay_second() {
    console.log("change second pay....");
    var order_report_price = parseInt($("#id_order_report_price").val());
    var pay_first = parseInt($("#id_pay_first").val());
    $("#id_pay_second").val((order_report_price - pay_first));


}

function load_client_info(client_id) {
    var promise = $.getJSON("/client/read/single?id=" + client_id);
    promise.then(function (result) {
        for (var i in result) {
            $("#id_deal_client_company").val(result[i].COMPANY_NAME);
            $("#id_deal_client_company_code").val(result[i].INVOICE_NUM);
            $("#id_deal_client_company_address").val(result[i].ADDRESS)
            $("#id_deal_client_name").val(result[i].CLIENT_NAME);
        }
    });


}

function update_qu_item(id) {
    $.ajax({
        url: "/deal/update/order_items",
        data: {
            order_id: id
        },
        type: "POST",
        dataType: 'json',
        success: function (msg) {
            // alert(+" 成功的存入！");
        },

        error: function (xhr, ajaxOptions, thrownError) {
            alert("err:" + xhr.status);
            alert("err:" + thrownError);
        }
    });
}


function save_client_info() {
    if (!$("#id_deal_client_id").val()) {
        $.ajax({
            url: "/client/create",
            data: {
                my_order_id: $("#id_order_id").val(),
                name: $("#id_deal_client_name").val(),
                company: $("#id_deal_client_company").val(),
                code: $("#id_deal_client_company_code").val(),
                address: $("#id_deal_client_company_address").val(),
                mphone: $("#id_deal_client_phone").val(),
            },
            type: "POST",
            dataType: 'json',
            success: function (msg) {
                $("#id_deal_client_id").val(msg.insertID);
                // alert(msg);
                // alert(+" 成功的存入！");
                location.href = "/deal/view_deal_all";
            },

            error: function (xhr, ajaxOptions, thrownError) {
                // alert("123"+xhr.status);
                // alert("223"thrownError);
            }
        });
    } else {
        location.href = "/deal/view_deal_all";
    }

}