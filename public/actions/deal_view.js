$(function () {

    $("#jsGrid").jsGrid({
            height: "100%",
            width: "100%",
            // filtering: true,
            editing: false,
            sorting: true,
            paging: true,
            autoload: true,
            rowClick: function (args) {
                var tmp_css = $("#jsGrid .highlight").attr("class");
                if (tmp_css != undefined) {
                    tmp_css = tmp_css.replace("highlight", '');
                    $("#jsGrid .highlight").attr("class", tmp_css);
                }

                var $row = this.rowByItem(args.item);
                $row.toggleClass("highlight");
                showDetailsDialog("Edit", args.item);
            },
            pageSize: 15,
            pageButtonCount: 5,

            // deleteConfirm: "Do you really want to delete the client?",

            controller: {
                loadData: function (filter) {


                    // server-side filtering
                    var d = $.Deferred();
                    $.ajax({
                        type: "GET",
                        url: "/deal/read?load_type=" + $("#load_type").val(),
                        data: filter,
                        dataType: "json"
                    }).done(function (result) {
                        // client-side filtering
                        // result = $.grep(result, function (item) {
                        //     // return item.SomeField === filter.SomeField;
                        //     return (!filter.ORDER_ID || item.ORDER_ID.indexOf(
                        //             filter.ORDER_ID) > -1) &&
                        //         (!filter.CLIENT_COMPANY || item.CLIENT_COMPANY.indexOf(
                        //             filter.CLIENT_COMPANY) > -1) &&
                        //         (!filter.CLIENT_PHONE || item.CLIENT_PHONE.indexOf(
                        //             filter.CLIENT_PHONE) > -1) &&
                        //         (!filter.CLIENT_NAME || item.CLIENT_NAME.indexOf(
                        //             filter.CLIENT_NAME) > -1)
                        //     // && (!filter.ID || item.ID == filter.ID);
                        //     // && (filter.Married === undefined || client.Married === filter.Married);

                        // });

                        d.resolve(result);
                    })


                    return d.promise();

                },

                insertItem: function (item) {},

                updateItem: function (item) {},

                deleteItem: function (item) {}
            },

            fields: [{
                name: "ORDER_ID",
                type: "text",
                title: "報價編號No.",
                width: 30,
                headercss: "myHeader_qu_order"
            },
                {
                    name: "DEAL_TITLE",
                    type: "text",
                    title: "案件名稱",
                    width: 30,
                    align: "center",
                    headercss: "myHeader_qu_order",

                },

                {
                    name: "COMPANY_NAME",
                    title: "委託公司",
                    width: 25,
                    align: "center",
                    headercss: "myHeader_qu_order",
                    type: "text"

                },
                {
                    name: "APPLY_STATUS",
                    title: "請款狀態",
                    width: 25,
                    align: "center",
                    headercss: "myHeader_qu_order",
                    type: "text",
                    itemTemplate: function (value, item) {
                        console.log("value:" + value);
                        console.log("item: " + item);

                        var result_status = "";
                        var pay_msg = ",已全付款";
                        var pay_msg_first = ",已付頭款";


                        var is_separate_pay = false;
                        if (item.PAY_MONEY_FIRST && item.PAY_MONEY_FIRST != "0" && item.PAY_MONEY_SECOND &&
                            item.PAY_MONEY_SECOND != "0") {
                            is_separate_pay = true;
                        }


                        // apply status
                        if (item.APPLY_STATUS) {
                            result_status = "已請款";
                        } else {
                            result_status = "尚未請款";
                        }

                        var flag = true;
                        //first pay
                        if (item.PAY_MONEY_FIRST && item.PAY_MONEY_FIRST != "0" && (item.PAY_MONEY_FIRST_STATUS !=
                                "1")) {
                            var head_msg = is_separate_pay ? " ,頭款" : " ,";
                            result_status = result_status + head_msg + "未付";
                            flag = false;
                        }

                        //first pay
                        if (item.PAY_MONEY_SECOND && item.PAY_MONEY_SECOND != "0" && (item.PAY_MONEY_SECOND_STATUS !=
                                "1")) {
                            var head_msg = is_separate_pay ? " ,尾款" : " ,";
                            result_status = result_status + head_msg + "未付";
                            flag = false;
                        }
                        if (flag) {
                            result_status = result_status + ",全已付";
                        }
                        return result_status;
                    }
                },
                // {
                //     name: "STATUS_NOW",
                //     title: "狀態",
                //     width: 20,
                //     align: "center",
                //     headercss: "myHeader_qu_order",
                //     type: "text",
                // },
                {
                    name: "MODIFY_TIME",
                    title: "報價修改時間",
                    width: 30,
                    align: "center",
                    headercss: "myHeader_qu_order",
                    type: "text",
                    itemTemplate: function (value) {
                        if (value) {
                            var d = new Date(value);
                            return d.toISOString().substring(0, 10);
                        }
                    }
                },
                {
                    name: "CREATE_TIME",
                    title: "建立時間",
                    width: 30,
                    align: "center",
                    headercss: "myHeader_qu_order",
                    type: "text",
                    itemTemplate: function (value) {
                        var d = new Date(value);
                        return d.toISOString().substring(0, 10);
                    }
                },

                // {
                //     name: "REPORT_PRICE",
                //     type: "number",
                //     title: "報價",
                //     width: 35,
                //     headercss: "myHeader_qu_order",
                //     itemTemplate: function (value) {
                //         var num = parseInt(value);
                //         return numberWithCommas(num);
                //     }
                // }
                // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
                // { name: "Married", type: "checkbox", title: "Is Married", sorting: false },
                // { type: "control", deleteButton: false, align: "center", width: '30', headercss: "myHeader_qu_order", clearFilterButton: true, modeSwitchButton: false }
            ]
        });


    var formSubmitHandler = $.noop;

    var saveClient = function (client, isNew) {
        // $.extend(client, {
        //     Name: $("#name").val(),
        //     Age: parseInt($("#age").val(), 10),
        //     Address: $("#address").val(),
        //     Country: parseInt($("#country").val(), 10),
        //     Married: $("#married").is(":checked")
        // });

        // $("#jsGrid").jsGrid(isNew ? "insertItem" : "updateItem", client);
        var form_data = $("#detailsDialog").serializeArray();
        $.ajax({
            url: "/cost/updatePartInfo",
            data: form_data,
            type: "POST",
            dataType: 'json',
            success: function (msg) {
                alert(+" 成功的存入！");
            },

            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        }).done(function (result) {
            // alert(result);

        });

        // $("#detailsDialog").dialog("close");
    };

    $("#revise_detail").click(function () {
        // var form_data = $("#detailsDialog").serializeArray();
        window.location = "/cost/revise_cost_detail?type=" + $("#id_PK").val();
        // $.ajax({
        //     url: "/cost/updatePartInfo",
        //     data: form_data,
        //     type: "POST",
        //     dataType: 'json',
        //     success: function (msg) {
        //         alert(msg + " 成功的存入！");
        //     },

        //     error: function (xhr, ajaxOptions, thrownError) {
        //         alert(xhr.status);
        //         alert(thrownError);
        //     }
        // }).done(function(result) {
        //     alert(result);

        // });
    });
    $("#btn_already_pay_first").click(function () {
        var d = $.Deferred();
        $.ajax({
            type: "POST",
            url: "/deal/update/status/pay_first",
            data: {
                deal_id: $("#id_deal_id").val()
            },
            dataType: "json"
        }).done(function (result) {
            console.log("here is single order..." + result);
            if (result.msg == "success") {
                alert("成功更新");
                window.location.reload();
            }
            d.resolve(result);
        })
    });
    $("#btn_already_pay_second").click(function () {
        var d = $.Deferred();
        $.ajax({
            type: "POST",
            url: "/deal/update/status/pay_second",
            data: {
                deal_id: $("#id_deal_id").val()
            },
            dataType: "json"
        }).done(function (result) {
            console.log("here is single order..." + result);
            if (result.msg == "success") {
                alert("成功更新");
                window.location.reload();

            }
            d.resolve(result);
        })
    });



    $("#btn_apply_money").click(function () {
        var d = $.Deferred();
        $.ajax({
            type: "POST",
            url: "/deal/update/status/apply",
            data: {
                deal_id: $("#id_deal_id").val()
            },
            dataType: "json"
        }).done(function (result) {
            console.log("here is single order..." + result);
            if (result.msg == "success") {
                alert("成功更新");
                window.location.reload();

            }
            d.resolve(result);
        })
    });

    // finished case

    $("#btn_finish").click(function () {
        var d = $.Deferred();
        $.ajax({
            type: "POST",
            url: "/deal/update/status",
            data: {
                deal_id: $("#id_deal_id").val()
            },
            dataType: "json"
        }).done(function (result) {
            if (result.msg == "success") {
                alert("成功結案");
                window.location.reload();

            }
            d.resolve(result);
        })
    });


    $("#redirect_to_generate").click(function () {
        location.href = "/deal/gen_file?order_id=" + $("#id_order_id").val() + "&deal_id=" + $(
            "#id_deal_id").val();
    });
    // document ready ---end
});

function parseValue(value) {

    if (value != "") {
        value = value.replace(",", "");
    } else {
        return 0;
    }

    value = parseInt(value);
    if (!isNaN(value)) {
        return value;
    } else return 0;

}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function showDetailsDialog(formTYpe, item) {
    var id = item.ORDER_ID;

    $("#id_order_id").val(id);
    $("#id_deal_id").val(item.DEAL_ID);
    if (item.PAY_MONEY_SECOND || item.PAY_MONEY_SECOND != "0") {
        $("#id_pay_second").val(item.PAY_MONEY_SECOND);

    } else {
        $("#id_pay_type").val("全額付款");

    }
    var total = 0;
    if (item.PAY_MONEY_FIRST) {
        total = total + parseInt(item.PAY_MONEY_FIRST);
    }

    if (item.PAY_MONEY_SECOND) {
        total = total + parseInt(item.PAY_MONEY_SECOND);
    }
    $("#id_report_price").val(total);
    $("#id_pay_first").val(item.PAY_MONEY_FIRST);
    $("#id_pay_type").val(type_pay[item.PAY_TYPE]);
    // item.PAY_MONEY_FIRST
    // $("#id_modify_type").val(item.ORDER_TYPE);
    // $("#id_status_history").val(decodeURIComponent(item.STATUS_HISTORY));
    $("#id_create_date").val(item.CREATE_TIME.substring(0, 10));
    var time = item.CREATE_TIME;
    var tax = item.TAX;
    var discount = item.DISCOUNT;
    $("#detailsDialog").dialog({
        title: item.DEAL_TITLE,
        position: {
            my: "0",
            at: "center",
            of: window
        },
        width: "500px"
    }).dialog("open");
    getOrderItem(id);

    // return d.promise();
}

function getOrderItem(id) {
    var d = $.Deferred();
    $.ajax({
        type: "POST",
        url: "/quotation/read/order_items/part_cost_info",
        data: {
            order_id: id
        },
        dataType: "json"
    }).done(function (result) {
        // clean
        $("#order_item_list").children().remove();
        for (var i in result) {
            var item_name = "";
            if (result[i].TYPE_REGION) {
                item_name = item_name + country[result[i].TYPE_REGION];
            }
            if (result[i].TYPE_MAIN) {
                item_name = item_name + type_main_map[result[i].TYPE_MAIN];
            }
            if (result[i].MID_STATUS_NAME) {
                item_name = item_name + result[i].MID_STATUS_NAME;
            }
            if (result[i].TYPE_PATTERN) {
                item_name = item_name + type_pattern_map[result[i].TYPE_PATTERN];
            }
            if (result[i].TYPE_STATUS) {
                item_name = item_name + type_status_map[result[i].TYPE_STATUS];
            }
            if (result[i].TYPE_OTHERS) {
                item_name = item_name + result[i].TYPE_OTHERS;
            }
            var html = "<input class=\"form-control\" type=\"text\" value=\"" + item_name +
                "\" disabled>";

            $("#order_item_list").append(html);

        }
        console.log("here is itmes in  order..." + result);
        d.resolve(result);
    })


}
