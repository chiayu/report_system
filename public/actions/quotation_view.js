$(function () {

    $("#jsGrid").jsGrid({
        height: "100%",
        width: "100%",
        filtering: true,
        editing: false,
        sorting: true,
        paging: true,
        autoload: true,
        rowClick: function (args) {
            var tmp_css = $("#jsGrid .highlight").attr("class");
            if (tmp_css != undefined) {
                tmp_css = tmp_css.replace("highlight", '');
                $("#jsGrid .highlight").attr("class", tmp_css);
            }

            var $row = this.rowByItem(args.item);
            $row.toggleClass("highlight");
            showDetailsDialog("Edit", args.item);
        },
        pageSize: 10,
        pageButtonCount: 5,

        // deleteConfirm: "Do you really want to delete the client?",

        controller: {
            loadData: function (filter) {

                // server-side filtering
                var d = $.Deferred();
                $.ajax({
                    type: "GET",
                    url: "/quotation/read?load_type=" + $("#load_type").val(),
                    data: filter,
                    dataType: "json"
                }).done(function (result) {
                    // client-side filtering
                    result = $.grep(result, function (item) {
                        // return item.SomeField === filter.SomeField;
                        return (!filter.ORDER_ID || item.ORDER_ID.indexOf(
                                filter.ORDER_ID) > -1) &&
                            (!filter.CLIENT_COMPANY || item.CLIENT_COMPANY.indexOf(
                                filter.CLIENT_COMPANY) > -1) &&
                            (!filter.CLIENT_PHONE || item.CLIENT_PHONE.indexOf(
                                filter.CLIENT_PHONE) > -1) &&
                            (!filter.CLIENT_NAME || item.CLIENT_NAME.indexOf(
                                filter.CLIENT_NAME) > -1)
                        // && (!filter.ID || item.ID == filter.ID);
                        // && (filter.Married === undefined || client.Married === filter.Married);

                    });

                    d.resolve(result);
                })


                return d.promise();

            },

            insertItem: function (item) {},

            updateItem: function (item) {},

            deleteItem: function (item) {}
        },

        fields: [{
                name: "ORDER_ID",
                type: "text",
                title: "報價編號No.",
                width: 30,
                headercss: "myHeader_qu_order"
            },
            {
                name: "CLIENT_COMPANY",
                type: "text",
                title: "公司名",
                width: 30,
                align: "center",
                headercss: "myHeader_qu_order",

            },

            {
                name: "CLIENT_NAME",
                title: "人名",
                width: 25,
                align: "center",
                headercss: "myHeader_qu_order",
                type: "text"

            },
            {
                name: "CLIENT_PHONE",
                title: "電話",
                width: 25,
                align: "center",
                headercss: "myHeader_qu_order",
                type: "text"
            },
            // {
            //     name: "STATUS_NOW",
            //     title: "狀態",
            //     width: 20,
            //     align: "center",
            //     headercss: "myHeader_qu_order",
            //     type: "text",
            // },
            {
                name: "MODIFY_TIME",
                title: "報價修改時間",
                width: 30,
                align: "center",
                headercss: "myHeader_qu_order",
                type: "text",
                itemTemplate: function (value) {
                    if (value) {
                        var d = new Date(value);
                        return d.toISOString().substring(0, 10);
                    }
                }
            },
            {
                name: "CREATE_TIME",
                title: "報價建立時間",
                width: 30,
                align: "center",
                headercss: "myHeader_qu_order",
                type: "text",
                itemTemplate: function (value) {
                    var d = new Date(value);
                    return d.toISOString().substring(0, 10);
                }
            },

            // {
            //     name: "REPORT_PRICE",
            //     type: "number",
            //     title: "報價",
            //     width: 35,
            //     headercss: "myHeader_qu_order",
            //     itemTemplate: function (value) {
            //         var num = parseInt(value);
            //         return numberWithCommas(num);
            //     }
            // }
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
            // { name: "Married", type: "checkbox", title: "Is Married", sorting: false },
            // { type: "control", deleteButton: false, align: "center", width: '30', headercss: "myHeader_qu_order", clearFilterButton: true, modeSwitchButton: false }
        ]
    });


    var formSubmitHandler = $.noop;


    var saveClient = function (client, isNew) {

        var form_data = $("#detailsDialog").serializeArray();
        $.ajax({
            url: "/cost/updatePartInfo",
            data: form_data,
            type: "POST",
            dataType: 'json',
            success: function (msg) {
                alert(+" 成功的存入！");
            },

            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        }).done(function (result) {
            // alert(result);

        });

        // $("#detailsDialog").dialog("close");
    };

    $("#revise_detail").click(function () {
        // var form_data = $("#detailsDialog").serializeArray();
        window.location = "/cost/revise_cost_detail?type=" + $("#id_PK").val();
        // $.ajax({
        //     url: "/cost/updatePartInfo",
        //     data: form_data,
        //     type: "POST",
        //     dataType: 'json',
        //     success: function (msg) {
        //         alert(msg + " 成功的存入！");
        //     },

        //     error: function (xhr, ajaxOptions, thrownError) {
        //         alert(xhr.status); 
        //         alert(thrownError); 
        //     }
        // }).done(function(result) {
        //     alert(result);

        // });
    });

    $("#level_2").text("報價單");
    $("#level_3").text("瀏覽");

    $("#revise_order").click(function () {
        alert($("#id_status_history").val());
        var d = $.Deferred();
        $.ajax({
            type: "POST",
            url: "/quotation/update/qu_order/status",
            data: {
                order_id: $("#id_order_id").val(),
                status_history: encodeURIComponent($("#id_status_history").val())
            },
            dataType: "json"
        }).done(function (result) {
            console.log("here is single order..." + result);
            if (result.msg == "success") {
                alert("成功更新");
                window.location.reload();
            }
            d.resolve(result);
        })
    });

    $("#revise_order_detail").click(function () {
        console.log("redirect to .....editing page");
        var is_edit = true;
        if ($("#load_type").val() == "finished") {
            is_edit = false;
        }
        location.href = "/quotation/edit_quotation" + "?genType=" + $("#id_modify_type").val() +
            "&order_id=" + $("#id_order_id").val() + "&is_editable=" + is_edit;

    });
    $("#redirect_to_deal").click(function () {
        location.href = "/deal/gen_deal?order_id=" + $("#id_order_id").val();
    });
    if ($("#load_type").val() == "finished") {
        $("#level3").html("完成的報價單");
        $("#revise_order").remove();
        $("#redirect_to_deal").remove();

    } else {
        $("#level3").html("尚未委任");


    }
    // document ready ---end
});

function parseValue(value) {

    if (value != "") {
        value = value.replace(",", "");
    } else {
        return 0;
    }

    value = parseInt(value);
    if (!isNaN(value)) {
        return value;
    } else return 0;

}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function showDetailsDialog(formTYpe, item) {
    var id = item.ORDER_ID;
    // var d = $.Deferred();
    // $.ajax({
    //     type: "POST",
    //     url: "/quotation/read/single",
    //     data: {order_id:id},
    //     dataType: "json"
    // }).done(function (result) {
    //     console.log("here is single order..." + result);
    //     d.resolve(result);
    // })

    //  $("#id_PK").val(client.ID);
    $("#id_report_price").val(numberWithCommas(item.REPORT_PRICE));
    $("#id_order_id").val(id);
    $("#id_modify_type").val(item.ORDER_TYPE);
    $("#id_status_history").val(decodeURIComponent(item.STATUS_HISTORY));
    $("#id_create_date").val(item.CREATE_TIME.substring(0, 10));
    var time = item.CREATE_TIME;
    var tax = item.TAX;
    var discount = item.DISCOUNT;
    $("#detailsDialog").dialog({
        title: "修正註解與其他費用",
        position: {
            my: "0",
            at: "center",
            of: window
        },
        width: "400px"
    }).dialog("open");
    getOrderItem(id);

    // return d.promise();
}

function getOrderItem(id) {
    var d = $.Deferred();
    $.ajax({
        type: "POST",
        url: "/quotation/read/order_items/part_cost_info",
        data: {
            order_id: id
        },
        dataType: "json"
    }).done(function (result) {
        // clean
        $("#order_item_list").children().remove();
        for (var i in result) {
            var item_name = "";
            if (result[i].TYPE_REGION) {
                item_name = item_name + country[result[i].TYPE_REGION];
            }
            if (result[i].TYPE_MAIN) {
                item_name = item_name + type_main_map[result[i].TYPE_MAIN];
            }
            if (result[i].MID_STATUS_NAME) {
                item_name = item_name + result[i].MID_STATUS_NAME;
            }
            if (result[i].TYPE_PATTERN) {
                item_name = item_name + type_pattern_map[result[i].TYPE_PATTERN];
            }
            if (result[i].TYPE_STATUS) {
                item_name = item_name + type_status_map[result[i].TYPE_STATUS];
            }
            if (result[i].TYPE_OTHERS) {
                item_name = item_name + result[i].TYPE_OTHERS;
            }
            var html = "<input class=\"form-control\" type=\"text\" value=\"" + item_name +
                "\" disabled>";

            $("#order_item_list").append(html);

        }
        console.log("here is itmes in  order..." + result);
        d.resolve(result);
    })


}