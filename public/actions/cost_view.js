$(function () {

    $("#jsGrid").jsGrid({
        height: "100%",
        width: "100%",
        filtering: true,
        editing: false,
        sorting: true,
        paging: true,
        autoload: true,
        rowClick: function (args) {
            var tmp_css = $("#jsGrid .highlight").attr("class");
            if (tmp_css != undefined) {
                tmp_css = tmp_css.replace("highlight", '');
                $("#jsGrid .highlight").attr("class", tmp_css);
            }

            var $row = this.rowByItem(args.item);
            $row.toggleClass("highlight");
            showDetailsDialog("Edit", args.item);
        },
        pageSize: 10,
        pageButtonCount: 5,

        // deleteConfirm: "Do you really want to delete the client?",

        controller: {
            loadData: function (filter) {


                // server-side filtering
                var d = $.Deferred();
                $.ajax({
                    type: "GET",
                    url: "/cost/read",
                    data: filter,
                    dataType: "json"
                }).done(function (result) {
                    // client-side filtering
                    result = $.grep(result, function (item) {
                        // return item.SomeField === filter.SomeField;
                        return (!filter.TYPE_MAIN || item.TYPE_MAIN.indexOf(
                                filter.TYPE_MAIN) > -1) &&
                            (!filter.TYPE_PATTERN || item.TYPE_PATTERN ===
                                filter.TYPE_PATTERN) &&
                            (!filter.TYPE_STATUS || item.TYPE_STATUS.indexOf(
                                filter.TYPE_STATUS) > -1) &&
                            (!filter.TYPE_REGION || country[item.TYPE_REGION]
                                .indexOf(filter.TYPE_REGION) > -1) &&
                            (!filter.ID || item.ID == filter.ID);
                        // && (filter.Married === undefined || client.Married === filter.Married);

                    });

                    d.resolve(result);
                })


                return d.promise();

            },

            insertItem: function (item) {
                return $.ajax({
                    type: "POST",
                    url: "/cost/create",
                    data: item
                });
            },

            updateItem: function (item) {
                return $.ajax({
                    type: "PUT",
                    url: "/cost/update",
                    data: item
                });
            },

            deleteItem: function (item) {
                return $.ajax({
                    type: "DELETE",
                    url: "/items",
                    data: item
                });
            }
        },

        fields: [{
                name: "ID",
                type: "text",
                title: "編號No.",
                width: 20,
                headercss: "myHeader"
            },
            {
                name: "TYPE_MAIN",
                title: "主類別",
                width: 20,
                align: "center",
                headercss: "myHeader",
                valueField: "Id",
                textField: "Name",
                type: "select",
                items: [{
                        Name: "all",
                        Id: ''
                    },
                    {
                        Name: "專利",
                        Id: "pattern"
                    },
                    {
                        Name: "商標",
                        Id: "trademark"
                    },
                    {
                        Name: "其他",
                        Id: "others"
                    }
                ],
                itemTemplate: function (value) {
                    //   console.log(this);
                    //   return "ggg";
                    console.log(value);
                    if (value == "pattern") {
                        return "專利";
                    } else if (value == "trademark") {
                        return "商標";
                    } else {
                        return "其他";
                    }

                }
            },
            {
                name: "TYPE_REGION",
                type: "text",
                title: "國別",
                width: 30,
                align: "center",
                headercss: "myHeader",

                itemTemplate: function (value) {
                    if (country != undefined) {
                        if (country[value] != undefined) {
                            return country[value];
                        }
                    }

                }
            },

            {
                name: "TYPE_PATTERN",
                title: "專利類別",
                width: 25,
                align: "center",
                headercss: "myHeader",
                valueField: "Id",
                textField: "Name",
                type: "select",
                items: [{
                        Name: "all",
                        Id: ''
                    },
                    {
                        Name: "設計",
                        Id: "design"
                    },
                    {
                        Name: "新型",
                        Id: "new"
                    },
                    {
                        Name: "發明",
                        Id: "invent"
                    },
                    {
                        Name: "其他",
                        Id: "others"
                    }
                ],
                itemTemplate: function (value) {
                    if (value == "design") {
                        return "設計";
                    } else if (value == "new") {
                        return "新型";
                    } else if (value == "invent") {
                        return "發明";
                    } else {
                        return "其他";
                    }
                }
            },
            {
                name: "TYPE_STATUS",
                title: "流程類別",
                width: 25,
                align: "center",
                headercss: "myHeader",
                valueField: "Id",
                textField: "Name",
                type: "select",
                items: [{
                        Name: "all",
                        Id: ''
                    },
                    {
                        Name: "新申請",
                        Id: "new_apply"
                    },
                    {
                        Name: "年費",
                        Id: "year_fee"
                    },
                    {
                        Name: "領證",
                        Id: "get_cert"
                    },
                    {
                        Name: "中間程序",
                        Id: "mid_process"
                    },
                    {
                        Name: "其他",
                        Id: "others"
                    }
                ],
                itemTemplate: function (value) {
                    if (value == "new_apply") {
                        return "新申請";
                    } else if (value == "year_fee") {
                        return "年費";
                    } else if (value == "get_cert") {
                        return "領證";
                    } else if (value == "mid_process") {
                        return "中間程序";
                    } else {
                        return "其他";
                    }
                }
            },
            {
                name: "MID_STATUS_NAME",
                title: "中間程序名",
                width: 20,
                align: "center",
                headercss: "myHeader",
                type: "text",
            },
            {
                name: "TYPE_OTHERS",
                title: "其他名",
                width: 20,
                align: "center",
                headercss: "myHeader",
                type: "text",
            },

            {
                name: "REPORT_PRICE",
                type: "number",
                title: "報價",
                width: 35,
                headercss: "myHeader",
                itemTemplate: function (value) {
                    var num = parseInt(value);
                    return numberWithCommas(num);
                }
            }
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
            // { name: "Married", type: "checkbox", title: "Is Married", sorting: false },
            // { type: "control", deleteButton: false, align: "center", width: '30', headercss: "myHeader", clearFilterButton: true, modeSwitchButton: false }
        ]
    });

    $("#detailsDialog").dialog({
        autoOpen: false,
        width: 500,
        title: "修正註解與其他費用",
        position: {
            my: "0",
            at: "center",
            of: window
        },
        close: function () {
            $("#detailsForm").validate().resetForm();
            $("#detailsForm").find(".error").removeClass("error");
        }
    });

    $("#detailsDialog").validate({

        submitHandler: function () {
            console.log("bind....");
            formSubmitHandler();
        }
    });

    var formSubmitHandler = $.noop;

    var showDetailsDialog = function (dialogType, client) {
        $("#id_PK").val(client.ID);
        $("#id_report_price").val(numberWithCommas(client.REPORT_PRICE));
        $("#id_note").val(decodeURIComponent(client.NOTE));
        $("#id_min_month").val(client.MIN_MONTH);
        $("#id_max_month").val(client.MAX_MONTH);
        $("#id_plus_priority").val(client.PLUS_PRIORITY);
        $("#id_plus_specific_country").val(client.PLUS_SPECIFIC_COUNTRY);
        $("#id_plus_real_check").val(client.PLUS_REAL_CHECK);

        formSubmitHandler = function () {
            console.log("submit...." + dialogType);
            saveClient(client, dialogType === "Edit");
            console.log("submit....");
        };

        $("#detailsDialog").dialog({
                title: "修正註解與其他費用",
                position: {
                    my: "0",
                    at: "center",
                    of: window
                },
            })
            .dialog("open");
    };

    var saveClient = function (client, isNew) {
        // $.extend(client, {
        //     Name: $("#name").val(),
        //     Age: parseInt($("#age").val(), 10),
        //     Address: $("#address").val(),
        //     Country: parseInt($("#country").val(), 10),
        //     Married: $("#married").is(":checked")
        // });

        // $("#jsGrid").jsGrid(isNew ? "insertItem" : "updateItem", client);
        var form_data = $("#detailsDialog").serializeArray();
        $.ajax({
            url: "/cost/updatePartInfo",
            data: form_data,
            type: "POST",
            dataType: 'json',
            success: function (msg) {
                alert(" 成功更新成本檔案！");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status);
                // alert(thrownError);
                alert("系統忙碌中");
            }
        }).done(function (result) {
            // alert(result);
            window.location.reload();// 重新整理
        });

        $("#detailsDialog").dialog("close");
    };

    $("#revise_detail").click(function () {
        window.location = "/cost/revise_cost_detail?type=" + $("#id_PK").val();
    });
});