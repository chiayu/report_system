$(function () {

    // =============DOCUMENT READY START
    // date
    var today = new Date();
    $("#form_date").val(today.toISOString().substring(0, 10));

    // load data......
    var order_id = $("#order_id").val();
    var deal_id = $("#deal_id").val();
    $("#id_order_id").val(order_id);
    var d = $.Deferred();
    var promise = $.getJSON("/deal/read/mix/single?deal_id=" + deal_id);
    promise.then(function (result) {
        // clean
        // $("#order_item_list").children().remove();
        var result_client_info_id = "";
        for (var i in result) {
            $("#id_order_report_price").val(result[i].REPORT_PRICE);
            if (parseInt(result[i].TAX) > 0) {
                $("#id_label_for_tax").text("價格含稅" + result[i].TAX);
                $("#id_input_for_tax").val(result[i].TAX);
            } else {
                $("#id_label_for_tax").text("不含稅" + result[i].TAX);
            }
            $("#id_deal_title").val(result[i].DEAL_TITLE);
            $("#id_pay_first").val(result[i].PAY_MONEY_FIRST);
            $("#id_pay_second").val(result[i].PAY_MONEY_SECOND);
            $("#id_pay_type").val(type_pay[result[i].PAY_TYPE])
            $("#id_account_text").val(result[i].ACCOUNT_TEXT);

            result_client_info_id = result[i].CID;
        }
        console.log("here is itmes in  order..." + result);
        d.resolve(result_client_info_id);
        return d;

    }).then(function (result) {

        // if (result[0].CLIENT_ID) {
        if (result) {
            console.log("load client info.....");
            load_client_info(result);
        }

    }).then(function (result2) {
        var order_id = $("#order_id").val();
        getOrderItem(order_id);
    });

    $("#confirm_deal").click(function () {
        console.log("uploading....");


        $("#deal_info").submit();

        var promise = $.getJSON("/deal/read/");
        promise.then(function (result) {
            // clean
            // $("#order_item_list").children().remove();
            for (var i in result) {
                console.dir(result[i]);
                $("#id_order_report_price").val(result[i].REPORT_PRICE)

            }
            console.log("here is itmes in  order..." + result);
            d.resolve(result);
            return result;
        });
    });

    // button
    $("#gen_receipt_cash").click(function () {
        if ($("#order_item_select input[type='radio']:checked").length < 1) {
            alert("請選擇要產生的收據");
            return;
        }
        var request_info = {};
        request_info['order_id'] = $("#id_order_id").val();
        // request_info['price'] = $("#id_pay_first").val();
        // request_info['item'] = $("#id_deal_title").val();
        request_info['company'] = $("#id_deal_client_company").val();
        request_info['client'] = $("#id_deal_client_name").val();

        // var gen_this = $("#order_item_select input[type='radio']:checked").parent().parent();
        var gen_class_id = $("#order_item_select input[type='radio']:checked").attr("class");
        gen_class_id = gen_class_id.replace(/form-control/, "").trim();
        var gen_this = $("." + gen_class_id);
        if (gen_this.length > 0) {
            request_info['item'] = gen_this.filter("#gen_item_name").val();
            request_info['code'] = gen_this.filter("#gen_item_code").val();
            request_info['price'] = gen_this.filter("#gen_item_my_price").val();
            var tables = [];

            gen_this.filter(".gen_item_my_ohters_t").each(function (idx, ele) {
                var a_item_info = {};
                if ($(ele).text().indexOf("稅") > -1) {
                    a_item_info['a_item_tax'] = $(ele).text();
                } else {
                    a_item_info['a_item_title'] = $(ele).text();
                    a_item_info['a_item_price'] = $(ele).next().text();
                }
                tables.push(a_item_info);
                console.log($(ele).next().text());
            });
            // gen_this.filter(".gen_item_my_ohters_p").each(function(idx,ele){ 
            //   console.log($(ele));
            // });
            request_info['tables'] = tables;
            request_info['item_price'];
        }
        console.dir(request_info);
        var promise = $.ajax({

            url: "/file/receipt_cash",
            data: JSON.stringify(request_info),
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            success: function (msg) {
                alert("success adding...." + msg.url);
                location.href = msg.url;
            },

            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status);
                alert(thrownError);
            }
        });

        promise.then(function () {
            console.log("1");
            // post_to_qu(request_info);
        }).then(function () {
            console.log("2");
            // post_to_qu_items(request_info);
        }).then(function () {
            console.log("3");

        });

    });

    // download_file
    $("#download_file").click(function () {
        location.href = "/temp/" + $("#id_order_id").val() + ".pdf";
    });
    // gene file
    $("#gen_receipt_for_company").click(function () {
        if ($("#order_item_select input[type='radio']:checked").length < 1) {
            alert("請選擇要產生的收據");
            return;
        }

        var request_info = {};
        request_info['order_id'] = $("#id_order_id").val();
        // request_info['price'] = $("#id_pay_first").val();
        // request_info['item'] = $("#id_deal_title").val();
        request_info['company'] = $("#id_deal_client_company").val();
        request_info['company_code'] = $("#id_deal_client_company_code").val();
        // request_info['code'] = 
        // var gen_this = $("#order_item_select input[type='radio']:checked").parent().parent();
        var gen_class_id = $("#order_item_select input[type='radio']:checked").attr("class");
        gen_class_id = gen_class_id.replace(/form-control/, "").trim();
        var gen_this = $("." + gen_class_id);
        if (gen_this.length > 0) {
            request_info['item'] = gen_this.filter("#gen_item_name").val();
            request_info['code'] = gen_this.filter("#gen_item_code").val();
            request_info['price'] = gen_this.filter("#gen_item_my_price").val();
        }
        request_info['client'] = $("#id_deal_client_name").val();

        console.dir(request_info);
        var promise = $.ajax({

            url: "/file/receipt_for_company",
            data: JSON.stringify(request_info),
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            success: function (msg) {
                alert("success adding...." + msg.url);
                location.href = msg.url;
            },

            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status);
                alert(thrownError);
            }
        });

        promise.then(function () {
            console.log("1");
            // post_to_qu(request_info);
        }).then(function () {
            console.log("2");
            // post_to_qu_items(request_info);
        }).then(function () {
            console.log("3");

        });

    });

    // button
    $("#gen_apply_money").click(function () {
        if ($("#order_item_select input[type='radio']:checked").length < 1) {
            alert("請選擇要產生的收據");
            return;
        }

        var request_info = {};
        request_info['order_id'] = $("#id_order_id").val();
        // request_info['price'] = $("#id_pay_first").val();
        request_info['deal_title'] = $("#id_deal_title").val();
        request_info['company'] = $("#id_deal_client_company").val();
        request_info['company_code'] = $("#id_deal_client_company_code").val();
        // request_info['code'] = 
        // var gen_this = $("#order_item_select input[type='radio']:checked").parent().parent();
        var gen_class_id = $("#order_item_select input[type='radio']:checked").attr("class");
        gen_class_id = gen_class_id.replace(/form-control/, "").trim();
        var gen_this = $("." + gen_class_id);
        if (gen_this.length > 0) {
            request_info['item_title'] = gen_this.filter("#gen_item_name").val();
            request_info['item_code'] = gen_this.filter("#gen_item_code").val();
            request_info['item_price'] = gen_this.filter("#gen_item_my_price").val();
            request_info['item_plus_note'] = gen_this.filter("#gen_item_info").text();
            var tables = [];
            gen_this.filter(".gen_item_my_ohters_t").each(function (idx, ele) {
                var a_item_info = {};
                if ($(ele).text().indexOf("稅") > -1) {
                    request_info['total_tax'] = "true";
                } else if ($(ele).text().indexOf("政府規費") > -1 && $(ele).text().indexOf("申請費") == -1) {
                    request_info['total_gov_fee'] = $(ele).next().text();
                } else {
                    a_item_info['a_item_title'] = $(ele).text();
                    a_item_info['a_item_price'] = $(ele).next().text();
                    tables.push(a_item_info);
                }
                console.log($(ele).next().text());
            });
            // gen_this.filter(".gen_item_my_ohters_p").each(function(idx,ele){ 
            //   console.log($(ele));
            // });
            request_info['tables'] = tables;
        }
        request_info['client'] = $("#id_deal_client_name").val();
        request_info['pay_account'] = $("#id_account_text").val();
        // request_info['client']
        console.dir(request_info);
        var promise = $.ajax({

            url: "/file/apply_money",
            data: JSON.stringify(request_info),
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            success: function (msg) {
                alert("success adding...." + msg.url);
                location.href = msg.url;
            },

            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status);
                alert(thrownError);
            }
        });

        promise.then(function () {
            console.log("1");
            // post_to_qu(request_info);
        }).then(function () {
            console.log("2");
            // post_to_qu_items(request_info);
        }).then(function () {
            console.log("3");

        });
    });

    // radio ===
    //event:
    $("#div_pay_book_type :input").change(function () {
        var price_pay_first = parseInt($("#id_order_report_price").val());
        if ($("#div_pay_book_type").parent().length > 0 &&
            ($("#div_pay_book_type input[type='radio']:checked").val() == "all")) {
            $("#id_pay_first").val(price_pay_first);
            count_pay_second();
        } else if ($("#div_pay_book_type input[type='radio']:checked").val() == "half") {
            // $("#form_pattern_type").parent().hide()
            $("#id_pay_first").val((price_pay_first / 2));
            count_pay_second();
        } else if ($("#div_pay_book_type input[type='radio']:checked").val() == "other") {

        }
        console.log(this); // points to the clicked input button
    });

    //event: 顯示
    $("#gen_file_price :input").change(function () {
        if (($("#gen_file_price input[type='radio']:checked").val() == "pay_first")) {
            $("#id_gen_total_price").val($("#id_pay_first").val());
        } else {
            $("#id_gen_total_price").val($("#id_pay_second").val());

        }
        console.log(this); // points to the clicked input button
    });

    // document ready ---end

});

function count_pay_second() {
    console.log("change second pay....");
    var order_report_price = parseInt($("#id_order_report_price").val());
    var pay_first = parseInt($("#id_pay_first").val());
    $("#id_pay_second").val((order_report_price - pay_first));


}

function load_client_info(client_id) {
    var promise = $.getJSON("/client/read/single?id=" + client_id);
    promise.then(function (result) {
        for (var i in result) {
            $("#id_deal_client_company").val(result[i].COMPANY_NAME);
            $("#id_deal_client_company_code").val(result[i].INVOICE_NUM)
        }
    });
}

function getOrderItem(id) {
    var d = $.Deferred();
    $.ajax({
        type: "GET",
        url: "/quotation/read/order_items/part_cost_info/code?id=" + id,
        // data: {
        //   order_id: id
        // },
        dataType: "json"
    }).done(function (result) {
        // clean
        $("#order_item_list").children().remove();
        $("#order_item_price_for_file").children().remove();
        $("#order_item_code").children().remove();
        $("#order_item_price").children().remove();
        $("#order_item_select").children().remove();
        $("#order_item_others").children().remove();


        for (var i in result) {
            var item_name = "";
            if (result[i].TYPE_REGION) {
                item_name = item_name + country[result[i].TYPE_REGION];
            }
            if (result[i].TYPE_MAIN) {
                item_name = item_name + type_main_map[result[i].TYPE_MAIN];
            }
            if (result[i].MID_STATUS_NAME) {
                item_name = item_name + result[i].MID_STATUS_NAME;
            }
            if (result[i].TYPE_PATTERN) {
                item_name = item_name + type_pattern_map[result[i].TYPE_PATTERN];
            }
            if (result[i].TYPE_STATUS) {
                item_name = item_name + type_status_map[result[i].TYPE_STATUS];
            }
            if (result[i].TYPE_OTHERS) {
                item_name = item_name + result[i].TYPE_OTHERS;
            }
            var r_code = result[i].CODE;
            var msg_include = "(含申請費";
            if (parseInt(result[i].GOV_FEE) > 0) {
                msg_include = msg_include + "含有政府規費,";
                var html_i = gen_html_others("申請費(含有政府規費)", result[i].ITEM_REPORT_PRICE, r_code);
                $("#order_item_others").append(html_i);
                html_i = gen_html_others("政府規費", result[i].GOV_FEE, r_code);
                $("#order_item_others").append(html_i);
            } else {
                var html_i = gen_html_others("申請費", result[i].ITEM_REPORT_PRICE, r_code);
                $("#order_item_others").append(html_i);
            }
            if (result[i].PLUS_INVALIDITY_SEARCH) {
                msg_include = msg_include + "含案前檢索費" + result[i].INVALIDITY_SEARCH + "元,";
                var html_i = gen_html_others("案前檢索費", result[i].INVALIDITY_SEARCH, r_code);
                $("#order_item_others").append(html_i);
            }
            if (result[i].PLUS_LARGE_ENTITY) {
                msg_include = msg_include + "含大實體費" + result[i].LARGE_ENTITY + "元,";
                var html_i = gen_html_others("大實體費", result[i].LARGE_ENTITY, r_code);
                $("#order_item_others").append(html_i);
            }
            if (result[i].PLUS_NOTARIZATION) {
                msg_include = msg_include + "含公證費用" + result[i].NOTARIZATION + "元,";
                var html_i = gen_html_others("公證費用", result[i].NOTARIZATION, r_code);
                $("#order_item_others").append(html_i);
            }
            if (result[i].PLUS_PRIORITY) {
                msg_include = msg_include + "含優先權" + result[i].PRIORITY + "元,";
                var html_i = gen_html_others("優先權加收", result[i].PRIORITY, r_code);
                $("#order_item_others").append(html_i);
            }
            if (result[i].PLUS_REAL_CHECK) {
                msg_include = msg_include + "含實審" + result[i].REAL_CHECK + "元,";
                var html_i = gen_html_others("實審加收", result[i].REAL_CHECK, r_code);
                $("#order_item_others").append(html_i);
            }
            if (result[i].PLUS_SMALL_ENTITY) {
                msg_include = msg_include + "小實體" + result[i].SMALL_ENTITY + "元,";
                var html_i = gen_html_others("小實體", result[i].SMALL_ENTITY, r_code);
                $("#order_item_others").append(html_i);

            }
            if (result[i].PLUS_SPECIFIC_COUNTRY) {
                msg_include = msg_include + "含有指定國家" + result[i].SPECIFIC_COUNTRY + "元,";
                var html_i = gen_html_others("指定國家加收", result[i].SPECIFIC_COUNTRY, r_code);
                $("#order_item_others").append(html_i);
            }
            if (result[i].PLUS_TINY_ENTITY) {
                msg_include = msg_include + "微實體" + result[i].TINY_ENTITY + "元,";
                var html_i = gen_html_others("微實體", result[i].TINY_ENTITY, r_code);
                $("#order_item_others").append(html_i);

            }
            if (result[i].PLUS_TRANSFER) {
                msg_include = msg_include + "含讓渡費" + result[i].TRANSFER + "元,";
                var html_i = gen_html_others("讓渡費", result[i].TRANSFER, r_code);
                $("#order_item_others").append(html_i);

            }
            if (result[i].PLUS_TRANSLATION) {
                msg_include = msg_include + "含翻譯費" + result[i].TRANSLATION + "元,";
                var html_i = gen_html_others("翻譯費", result[i].TRANSLATION, r_code);
                $("#order_item_others").append(html_i);
            }
            if ($("#id_input_for_tax").val() != "0") {
                msg_include = msg_include + "含稅 ";
                var html_i = gen_html_others("含稅", "", r_code);
                $("#order_item_others").append(html_i);
            }
            msg_include = msg_include + " )";
            var html_msg_label = " <div id=\"gen_item_info\" style=\"color:brown\">" +
                msg_include + "</div><br>";

            var html = "<input id=\"gen_item_name\" class=\"form-control " + result[i].CODE + "\" type=\"text\" value=\"" + item_name +
                "\" disabled><br>";

            var html_inpyt = "<input id=\"gen_item_my_price\" class=\"form-control " + result[i].CODE + "\" onchange=\"count_total_price()\" type=\"number\" value=\"0" +
                "\" id=" + result[i].ID + "><br>";

            var html_code = "<input id=\"gen_item_code\" class=\"form-control " + result[i].CODE + "\" type=\"text\" value=\"" + result[i].CODE +
                "\" disabled><br>";

            var html_price = "<input id=\"gen_item_price\" class=\"form-control " + result[i].CODE + "\" type=\"number\" value=\"" + result[i].ITEM_REPORT_PRICE +
                "\" disabled><br>";

            var html_sel = " <input type=\"radio\" class=\"form-control " + result[i].CODE + "\" name=\"i_gen_file_select\" value=\"none\"><br>";

            $("#order_item_select").append(html_sel);
            $("#order_item_list").append(html);
            $("#order_item_price_for_file").append(html_inpyt);

            // $("#order_item_price").append(html_price);
            $("#order_item_price").append(html_msg_label);
            $("#order_item_code").append(html_code);

        }
        console.log("here is itmes in  order..." + result);
        d.resolve(result);
    })


}

function count_total_price() {
    var total = 0;
    $("#order_item_price_for_file input").each(function (i, item) {
        if (item.value && parseInt(item.value) > 0) {
            total = total + parseInt(item.value);
        };
    });
    var total_gen = parseInt($("#id_gen_total_price").val());
    var sub = total_gen - total;
    if (total != total_gen) {
        $("#alert_price").text("價格相加不對");
        $("#alert_price").css("color", "red");
    } else {
        $("#alert_price").text("價格相加正確");
        $("#alert_price").css("color", "blue");

    }
}

function count_lastone() {
    var total = 0;
    $("#order_item_price_for_file input").each(function (i, item) {
        if (item.value && parseInt(item.value) > 0) {
            total = total + parseInt(item.value);
        };
    });
    var total_gen = parseInt($("#id_gen_total_price").val());
    var sub = total_gen - total;
    $("#order_item_price_for_file input").last().val(sub);
    count_total_price()
}

function gen_html_others(item_name, price, code) {
    var html_inpyt = "<li class=\"gen_item_my_ohters_t " + code + "\" >" + item_name +
        "</li>" + "<span class=\"gen_item_my_ohters_p " + code + "\">" + price + "</span>";
    return html_inpyt;
}