var type_pattern_map = { 'design':"設計",
                         'new':'新型',
                          'invent':'發明',
                          'others':'其他'}

var type_status_map = { 'new_apply':"新申請",
                         'get_cert':'領證',
                         'mid_process':'中間程序',
                         'others':'其他',
                          'year_fee':'年費'}

var type_main_map = { 'pattern':"專利",
                         'trademark':'商標',
                          'others':'其他'}


var type_pay = { 'all':"全額支付",
                         'half':'先付款二分之一',
                         'end':'尾款',
                          'other':'其他'}