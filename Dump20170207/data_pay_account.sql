/*
-- Query: SELECT * FROM report_system.PAY_ACCOUNT
LIMIT 0, 1000

-- Date: 2017-11-09 20:03
*/
INSERT INTO `PAY_ACCOUNT` (`ID`,`BANK_NAME`,`BANK_ID`,`BANK_BRANCH`,`ACCOUNT_NUM`,`ACCOUNT_NAME`) VALUES (1,'國泰世華','013','大安分行','020-03-500469-7','合曜國際專利商標事務所陳思合');
INSERT INTO `PAY_ACCOUNT` (`ID`,`BANK_NAME`,`BANK_ID`,`BANK_BRANCH`,`ACCOUNT_NUM`,`ACCOUNT_NAME`) VALUES (2,'國泰世華','013','信義分行','015-50-610233-0','陳慶璁');
