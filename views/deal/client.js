var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var connections = require('../connections');


/* API for client,  CM_CLIENT_INFO  */

router.get("/read", function (req, res) {
  res.setHeader('Content-Type', 'application/json');

  //========= CONNECTION START ========
  connections.getConnection(function (err, c) {

    c.query('SELECT * FROM  CM_CLIENT_INFO ', function (error, rows) {
      c.release();
      if (error) {
        console.log(error);
        res.status(500).send({
          "msg": "error",
          errorCode: "ERR_CLIENT_002"
        });
      } else {
        res.status(200).send(rows);
      }
    });
  });
  //========= CONNECTION END ========
});



router.get("/read/single", function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  var pk = "";
  if (req.query != null) {
    pk = req.query.id;
  }
  //========= CONNECTION START ========
  connections.getConnection(function (err, c) {

    c.query('SELECT * FROM  CM_CLIENT_INFO WHERE ID = ? ',[pk], function (error, rows) {
      c.release();
      if (error) {
        console.log(error);
        res.status(500).send({
          "msg": "error",
          errorCode: "ERR_CLIENT_003"
        });
      } else {
        res.status(200).send(rows);
      }
    });
  });
  //========= CONNECTION END ========
});



router.post('/create', function (req, res, next) {
  var requestbody = req.body;

  var post = {
    COMPANY_NAME: requestbody.company,
    CLIENT_NAME: requestbody.name,
    ADDRESS: requestbody.address,
    MPHONE: requestbody.mphone,
    INVOICE_NUM: requestbody.code
  };


  console.log("posting....");
  connections.getConnection(function (err, c) {

    c.query('INSERT INTO CM_CLIENT_INFO SET ?', post, function (error,rows) {
      // c.release();
      if (error) {
        console.log(error.message);
        res.status(500).send({"msg":"error", "errorCode":"ERR_CLEN"});
      } 
        var post_2 = { client_id : rows.insertId };
        var ids = { order_id : requestbody.my_order_id };

        c.query('UPDATE QU_DEAL SET ? where ?', [post_2, ids], function (error) {
          c.release();
          if (error) {
            console.log(error.message);
            res.status(500).send({
              msg: "error",
              error_code: "QU_DEAL_0ddd6"
            });
          } 
        });
        console.log('success');
        res.status(200).send({"msg": "success", "insertID": rows.insertId});
    });
  });
  // connection string end

});

module.exports = router;