This is a report system for generating Microsoft Word files. 

### Env ###

* NodeJS 6.9.2
* MySQL 14.14 Distrib 5.7.17
* Ubuntu 14

### Start ###

* pm2 start process.json

### Build Preparation ###

* Import DB schema
* Import DB Data
* Install npm modules
* Install pm2
* Set connection configuration in connection.js
* Set port in bin/www(default expressJS:3000)


### Lack ###

* Webpack package
* Middleware